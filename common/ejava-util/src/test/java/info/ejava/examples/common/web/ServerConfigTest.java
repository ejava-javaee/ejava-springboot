package info.ejava.examples.common.web;

import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;

public class ServerConfigTest {
    @Test
    void can_format_default() throws URISyntaxException {
        //given
        ServerConfig cfg = new ServerConfig();
        //when
        cfg = cfg.build();
        //then
        BDDAssertions.then(cfg.getBaseUrl()).isEqualTo(new URI("http://localhost:8080"));
    }

    @Test
    void can_format_port() throws URISyntaxException {
        //given
        ServerConfig cfg = new ServerConfig().withPort(1234);
        //when
        cfg = cfg.build();
        //then
        BDDAssertions.then(cfg.getBaseUrl()).isEqualTo(new URI("http://localhost:1234"));
    }

    @Test
    void can_format_https_default() throws URISyntaxException {
        //given
        ServerConfig cfg = new ServerConfig()
                .withScheme("https")
                .withHost("aHost");
        //when
        cfg = cfg.build();
        //then
        BDDAssertions.then(cfg.getBaseUrl()).isEqualTo(new URI("https://aHost:8443"));
    }
}
