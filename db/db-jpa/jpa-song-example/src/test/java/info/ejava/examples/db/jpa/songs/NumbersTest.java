package info.ejava.examples.db.jpa.songs;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class NumbersTest {
    @Test
    void test17() {
        int value = 17;
        System.out.println("It is " + calcMood(value) + " for " + value);
/*
result=89
results=[50, 25, 29, 85, 89, 145, 42, 20, 4, 16, 37, 58]
It is Sad for 17
 */
    }

    String calcMood(int value) {
        List<Integer> results = new ArrayList<>();
        int result = calcResult(value);
        while (result!= 1 && !results.contains(result)) {
            results.add(result);
            result = calcResult(result);
        }
        System.out.println("result=" + result);
        System.out.println("results=" + results);
        return result==1 ? "Happy" : "Sad";
    }

    int calcResult(Integer value) {
        return Arrays.stream(value.toString().split(""))
                .map(str -> Integer.valueOf(str))
                .map(d -> d * d)
                .reduce((r, l) -> r + l)
                .get();
    }
}
