package info.ejava.examples.db.validation.contacts;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.inject.Named;
import jakarta.validation.ParameterNameProvider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class MyParameterNameProvider implements ParameterNameProvider {
    private List<Function<Parameter, String>> parameterNameProviders = List.of(
            (p)->getNamedName(p),        //from a custom annotation
            (p)->getPathVariableName(p), //from Spring MVC path parameter annotation
            (p)->getRequestParamName(p)  //from Spring MVC query parameter annotation
    );

    @Override
    public List<String> getParameterNames(Constructor<?> ctor) {
        return getParameterNames((Executable) ctor);
    }

    @Override
    public List<String> getParameterNames(Method method) {
        return getParameterNames((Executable) method);
    }

    protected List<String> getParameterNames(Executable method) {
        List<String> argNames = new ArrayList<>(method.getParameterCount());
        for (Parameter param : method.getParameters()) {
            String argName = parameterNameProviders.stream()
                    .map(provider->provider.apply(param))
                    .filter(name->null!=name)
                    .findFirst()
                    .orElse(param.getName()); //default to Java reflection parameter name
            argNames.add(argName);
        }
        return argNames;
    }

    //custom naming
    String getNamedName(Parameter p) {
        Named annotation = p.getAnnotation(Named.class);
        return null == annotation ? null : annotation.value();
    }

    //Web MVC Controller path variable
    String getPathVariableName(Parameter p) {
        PathVariable annotation = p.getAnnotation(PathVariable.class);
        return null == annotation ? null : annotation.name();
    }
    //Web MVC Controller query parameter
    String getRequestParamName(Parameter p) {
        RequestParam annotation = p.getAnnotation(RequestParam.class);
        return null == annotation ? null : annotation.value();
    }
}
