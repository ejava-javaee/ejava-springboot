package info.ejava.examples.db.validation.contacts.dto;

import lombok.*;

import jakarta.validation.Payload;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;
import java.time.LocalDate;
import java.util.List;

@Builder
@Data
@With
@NoArgsConstructor
@AllArgsConstructor
@PersonHasName
public class PersonPocDTO {
    @Null(groups = PocValidationGroups.Create.class,
        message = "cannot be specified for create")
    private String id;
    private String firstName;
    private String lastName;
    @Past(groups = Default.class)
    private LocalDate dob;
    @Size(min=1, message = "must have at least one contact point")
    private List<@NotNull @Valid ContactPointDTO> contactPoints;
}
