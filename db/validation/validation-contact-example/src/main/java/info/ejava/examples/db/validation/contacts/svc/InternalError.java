package info.ejava.examples.db.validation.contacts.svc;

import jakarta.validation.Payload;

public interface InternalError extends Payload {}
