package info.ejava.examples.db.validation.contacts.dto;

import jakarta.validation.GroupSequence;
import jakarta.validation.groups.Default;

public interface PocValidationGroups {
    interface Create {}
    interface Update {}
    interface CreatePlusDefault extends Create, Default{}
    interface UpdatePlusDefault extends Update, Default{}

    interface SimplePlusDefault extends Default {}
    interface DetailedOnly {}

    @GroupSequence({ SimplePlusDefault.class, DetailedOnly.class })
    interface DetailOrder {}
}
