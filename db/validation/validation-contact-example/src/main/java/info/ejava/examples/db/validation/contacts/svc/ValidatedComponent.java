package info.ejava.examples.db.validation.contacts.svc;

import org.springframework.validation.annotation.Validated;

import jakarta.inject.Named;
import jakarta.validation.constraints.Min;

@Validated
public interface ValidatedComponent {
//    void aCall(@Min(5) @Named("mustBeGE5") int mustBeGE5);
    void aCall(@Min(5) int mustBeGE5);
}
