package info.ejava.examples.db.validation.contacts;

import de.flapdoodle.embed.mongo.spring.autoconfigure.EmbeddedMongoAutoConfiguration;
import info.ejava.examples.common.web.RestTemplateLoggingFilter;
import info.ejava.examples.common.web.ServerConfig;
import info.ejava.examples.common.webflux.WebClientLoggingFilter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.codec.xml.Jaxb2XmlDecoder;
import org.springframework.http.codec.xml.Jaxb2XmlEncoder;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

/**
 * This configuration replaces the main application configuration
 * so that different implementations of the beans can be turned on/off
 * based on what the tests are demonstrating.
 */
@SpringBootConfiguration
@ComponentScan(basePackageClasses = ValidationContactsApp.class,
    //https://javabydeveloper.com/filter-types-in-spring-componentscan/
    excludeFilters = {
        @ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE, classes={
                ValidationContactsApp.class, //eliminate overall production config defaults
                ContactsConfiguration.class, //allow test-specific tweaks to contacts
        }), //ignore any @TestConfiguration not explicitly named
        @ComponentScan.Filter(type=FilterType.ANNOTATION,
                classes = TestConfiguration.class)
})
@EnableAutoConfiguration
@EnableMongoRepositories
public class NTestConfiguration {

    @Bean
    public WebClient webClient(WebClient.Builder builder) {
        return builder
                .filter(WebClientLoggingFilter.requestFilter())
                .filter(WebClientLoggingFilter.responseFilter())
                .exchangeStrategies(ExchangeStrategies.builder().codecs(conf->{
                    conf.defaultCodecs().jaxb2Encoder(new Jaxb2XmlEncoder());
                    conf.defaultCodecs().jaxb2Decoder(new Jaxb2XmlDecoder());
                }).build())
                .build();
    }

    public static WebTestClient webTestClient(Integer port) {
        ServerConfig serverConfig = new ServerConfig();
        if (port!=null) {
            serverConfig.withPort(port);
        }
        serverConfig.build();
        return WebTestClient.bindToServer()
                .baseUrl(serverConfig.getBaseUrl().toString())
                .filter(WebClientLoggingFilter.requestFilter())
                .filter(WebClientLoggingFilter.responseFilter())
                .codecs(conf->{
                    conf.defaultCodecs().jaxb2Encoder(new Jaxb2XmlEncoder());
                    conf.defaultCodecs().jaxb2Decoder(new Jaxb2XmlDecoder());
                })
                .responseTimeout(Duration.ofDays(1))
                .build();
    }

    @Bean
    public WebTestClient webTestClient() {
        return webTestClient(null);
    }

    @Bean
    public ClientHttpRequestFactory requestFactory() {
        return new SimpleClientHttpRequestFactory();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder, ClientHttpRequestFactory requestFactory) {
        return builder.requestFactory(
                //used to read the streams twice -- so we can use the logging filter below
                ()->new BufferingClientHttpRequestFactory(requestFactory))
                .interceptors(new RestTemplateLoggingFilter())
                .build();
    }

    @Configuration
    @ConditionalOnProperty(prefix = "spring.data.mongodb", name = "uri", matchIfMissing = false)
    @EnableAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
    public class DisableEmbeddedMongoConfiguration {
    }

    @Bean @Lazy
    public ServerConfig serverConfig(@LocalServerPort int port) {
        return new ServerConfig().withPort(port).build();
    }
}
