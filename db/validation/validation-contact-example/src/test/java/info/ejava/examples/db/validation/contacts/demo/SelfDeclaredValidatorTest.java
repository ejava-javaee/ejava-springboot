package info.ejava.examples.db.validation.contacts.demo;

import info.ejava.examples.db.validation.contacts.MyParameterNameProvider;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Valid;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.executable.ExecutableValidator;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.util.Set;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * This test demonstrates that the
 */
@Slf4j
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class SelfDeclaredValidatorTest {
    private static Validator validator;
    private static ExecutableValidator executableValidator;

    @BeforeAll
    static void init() {
        //ValidatorFactory myValidatorFactory = Validation.buildDefaultValidatorFactory();
        ValidatorFactory myValidatorFactory = Validation.byDefaultProvider()
                .configure()
                .parameterNameProvider(new MyParameterNameProvider())
                .buildValidatorFactory();
        validator = myValidatorFactory.getValidator();
        executableValidator = validator.forExecutables();
    }

    @Getter
    private static class AClass {
        @NotNull
        private String aValue;
    }

    private static class AService {
        @NotNull
        public String aMethod(@NotNull @Valid AClass aParameter) {
//        public String aMethod(@NotNull @Valid @Named("aParameter") AClass aParameter) {
            return System.getProperty(aParameter.getAValue());
        }
    }

    @Test
    void self_declared_validator() {
        //given - a validator instantiated on the fly
        ValidatorFactory myValidatorFactory = Validation.buildDefaultValidatorFactory();
        Validator myValidator = myValidatorFactory.getValidator();
        AClass invalidAClass = new AClass();

        //when
        Set<ConstraintViolation<AClass>> violations = myValidator.validate(invalidAClass);
        violations.forEach(v->log.info("field name={}, value={}, violated={}, d={}",
                v.getPropertyPath(), v.getInvalidValue(), v.getMessage(), v.getConstraintDescriptor()));
        //then
        then(violations).isNotEmpty();
    }

    @Test
    void initialized_validator() {
        //given
        AClass invalidAClass = new AClass();

        //when
        Set<ConstraintViolation<AClass>> violations = validator.validate(invalidAClass);
        violations.forEach(v->log.info("field name={}, value={}, violated={}",
                v.getPropertyPath(), v.getInvalidValue(), v.getMessage()));
        //then
        then(violations).isNotEmpty();
    }

    @Test
    void method_arguments() throws NoSuchMethodException {
        //given
        AService myService = new AService();
        AClass myValue = new AClass();
        Object[] methodParams = new Object[]{ myValue };
        Class<?>[] methodParamTypes = new Class<?>[]{ AClass.class };
        Method methodToCall = AService.class.getMethod("aMethod", methodParamTypes);
        //when
        Set<ConstraintViolation<AService>> violations = validator
                .forExecutables().validateParameters(myService, methodToCall, methodParams);
        //then
        then(violations).hasSize(1);
        ConstraintViolation<?> violation = violations.iterator().next();
//        then(violation.getPropertyPath().toString()).isEqualTo("aMethod.arg0.aValue");
        then(violation.getPropertyPath().toString()).isEqualTo("aMethod.aParameter.aValue");
        then(violation.getMessage()).isEqualTo("must not be null");
        then(violation.getInvalidValue()).isEqualTo(myValue.getAValue());

        //given
        String nullResult = null;
        //when
        violations = validator.forExecutables()
                .validateReturnValue(myService, methodToCall, nullResult);
        //then
        then(violations).hasSize(1);
        violation = violations.iterator().next();
        then(violation.getPropertyPath().toString()).isEqualTo("aMethod.<return value>");
        then(violation.getMessage()).isEqualTo("must not be null");
        then(violation.getInvalidValue()).isEqualTo(nullResult);
    }

    @ParameterizedTest
    @CsvSource(nullValues = "null", value={"null","some text"})
    void can_validate_property(String aValue) {
        //given
        AClass aClass = new AClass();
        ReflectionTestUtils.setField(aClass,"aValue", aValue);
        //when
        Set<ConstraintViolation<AClass>> violations = validator.validateProperty(aClass, "aValue");
        //then
        if (null==aValue) {
            then(violations).hasSize(1);
            ConstraintViolation<?> violation = violations.iterator().next();
            then(violation.getPropertyPath().toString()).isEqualTo("aValue");
            then(violation.getMessage()).isEqualTo("must not be null");
            then(violation.getInvalidValue()).isNull();
        } else {
            then(violations).isEmpty();
        }
    }

    @ParameterizedTest
    @CsvSource(nullValues = "null", value={"null","some text"})
    void can_validate_value(String aValue) {
        //when
        Set<ConstraintViolation<AClass>> violations = validator.validateValue(AClass.class, "aValue", aValue);
        //then
        if (null==aValue) {
            then(violations).hasSize(1);
            ConstraintViolation<?> violation = violations.iterator().next();
            then(violation.getPropertyPath().toString()).isEqualTo("aValue");
            then(violation.getMessage()).isEqualTo("must not be null");
            then(violation.getInvalidValue()).isNull();
        } else {
            then(violations).isEmpty();
        }
    }
}
