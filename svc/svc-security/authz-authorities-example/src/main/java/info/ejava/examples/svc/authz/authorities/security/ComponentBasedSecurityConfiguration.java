package info.ejava.examples.svc.authz.authorities.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.NullRoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.sql.DataSource;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;
import static org.springframework.security.web.util.matcher.RegexRequestMatcher.regexMatcher;

@Configuration(proxyBeanMethods = false)
@EnableMethodSecurity(
        prePostEnabled = true, //@PreAuthorize("hasAuthority('ROLE_ADMIN')"), @PreAuthorize("hasRole('ADMIN')")
        jsr250Enabled = true,  //@RolesAllowed({"MANAGER"})
        securedEnabled = true  //@Secured({"ROLE_MEMBER"})
)
@RequiredArgsConstructor
public class ComponentBasedSecurityConfiguration {
    @Bean
    public WebSecurityCustomizer authzStaticResources() {
        return (web) -> web.ignoring().requestMatchers("/content/**");
    }

    @Bean
    @Order(0)
    public SecurityFilterChain authzSecurityFilters(HttpSecurity http,
                                                    RoleHierarchy roleHierarchy) throws Exception {
        http.securityMatchers(cfg->cfg.requestMatchers("/api/**"));

        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/whoAmI",
                "/api/authorities/paths/anonymous/**")
                .permitAll());

        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/authorities/paths/admin/**")
                .hasRole("ADMIN")
        );
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/authorities/paths/clerk/**")
                .hasAnyRole("ADMIN", "CLERK") //explicit ADMIN not needed with inheritance
        );
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/authorities/paths/customer/**")
                .hasAnyRole("CUSTOMER")
        );
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                HttpMethod.GET, "/api/authorities/paths/price")
//                .access(AuthorizationManagers.anyOf(
//                        AuthorityAuthorizationManager.hasAuthority("PRICE_CHECK"),
//                        AuthorityAuthorizationManager.hasRole("ADMIN"),
//                        AuthorityAuthorizationManager.hasRole("CLERK");
//                )));
                .hasAnyAuthority("PRICE_CHECK", "ROLE_ADMIN", "ROLE_CLERK"));
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/authorities/paths/nobody/**")
                .denyAll());
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                "/api/authorities/paths/authn/**")
                .authenticated()); //thru customizer.builder

        //these requests are handled by class/method annotations
        http.authorizeHttpRequests(cfg->cfg
                .requestMatchers("/api/authorities/secured/**",
                        "/api/authorities/jsr250/**",
                        "/api/authorities/expressions/**")
                .permitAll());

        http.httpBasic(cfg->cfg.realmName("AuthzExample"));
        http.formLogin(cfg->cfg.disable());
        http.headers(cfg->cfg.disable()); //disabling all security headers
//        http.headers(cfg->{ //disabling individually
//            cfg.frameOptions(fo->fo.disable());
//            cfg.xssProtection(xss->xss.disable());
//            cfg.cacheControl(cache-> cache.disable());
//            cfg.contentTypeOptions(content->content.disable());
//        });
        http.csrf(cfg->cfg.disable());
        http.cors(cfg-> cfg.configurationSource(req -> new CorsConfiguration().applyPermitDefaultValues()));
        http.sessionManagement(cfg->cfg.sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            HttpSecurity http, UserDetailsService jdbcUserDetailsService) throws Exception {
        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class);
        builder.userDetailsService(jdbcUserDetailsService);
        return builder.build();
    }

    @Bean
    @Order(500)
    public SecurityFilterChain h2SecurityFilters(HttpSecurity http,
             AuthenticationManager authenticationManager) throws Exception {
        http.securityMatchers(cfg->cfg
                .requestMatchers("/h2-console*",
                    "/h2-console/**")
                .requestMatchers(
                    "/login",
                    "/logout",
                    "/error")
        );
        http.authorizeHttpRequests(cfg->cfg
                 //error page must be explicitly permitted for favicon.ico 404 errors
                .requestMatchers("/error").permitAll()
                .requestMatchers(regexMatcher(HttpMethod.GET,".+(.css|.jsp|.gif)$")).permitAll()
                .anyRequest().authenticated()
        );
        http.formLogin(cfg->cfg
                .permitAll() //enables access to login and logout
                .successForwardUrl("/h2-console")
        );
        http.csrf(cfg->cfg.ignoringRequestMatchers(
                antMatcher("/h2-console/**")
        ));
        http.headers(cfg->{
            cfg.frameOptions(fo-> fo.sameOrigin());
        });

        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class);
        builder.parentAuthenticationManager(authenticationManager);
        return http.build();
    }

    protected static class AuthzCorsConfigurationSource implements CorsConfigurationSource {
        @Override
        public CorsConfiguration getCorsConfiguration(HttpServletRequest request) {
            return new CorsConfiguration().applyPermitDefaultValues();
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public UserDetailsService jdbcUserDetailsService(DataSource userDataSource) {
        JdbcDaoImpl jdbcUds = new JdbcDaoImpl();
        jdbcUds.setDataSource(userDataSource);
        return jdbcUds;
    }

    //needed mid-way thru lecture
    @Bean
    @Profile("roleInheritance")
    static RoleHierarchy roleHierarchy() {
        return RoleHierarchyImpl.withDefaultRolePrefix()
                .role("ADMIN").implies("CLERK")
                .role("CLERK").implies("CUSTOMER")
                .build();
//legacy
//        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
//        roleHierarchy.setHierarchy(StringUtils.join(List.of(
//                "ROLE_ADMIN > ROLE_CLERK",
//                "ROLE_CLERK > ROLE_CUSTOMER"
//        ),System.lineSeparator()));
//        return roleHierarchy;
    }

    /**
     * Creates a default RoleHierachy when the examples want straight roles.
     */
    @Bean
    @Profile("!roleInheritance")
    static RoleHierarchy nullHierarchy() {
        return new NullRoleHierarchy();
    }

    /**
     * Creates a custom MethodExpressionHandler that will be picked up by
     * Expression-based security to support RoleInheritance.
     * This is required until the
     * <a href="github.com/spring-projects/spring-security/issues/12783">the following</a>
     * is resolved.
     */
    //we are using Spring 6.1.11 with ***Spring Security 6.3.1***
    //@Bean
    static MethodSecurityExpressionHandler methodSecurityExpressionHandler(RoleHierarchy roleHierarchy, ApplicationContext context) {
        DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setRoleHierarchy(roleHierarchy);
        expressionHandler.setApplicationContext(context);
        return expressionHandler;
    }
}
