package info.ejava.examples.svc.authn.users;

import org.assertj.core.api.BDDAssumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserDetailsMockMvcNTest {
    @Autowired
    private MockMvc mockMvc;
    private String anonymousHelloURI = "/api/anonymous/hello";
    private String authnHelloURI = "/api/authn/hello";
    private String staticResourceURI = "/content/hello_static.txt";

    @BeforeEach
    void given() {
        BDDAssumptions.given(mockMvc).as("launch test from child").isNotNull();
    }

    @Nested
    class static_resource {
        @Test
        @WithAnonymousUser
        void can_be_accessed_anonymous() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.get(staticResourceURI))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andDo(MockMvcResultHandlers.print());
        }
    }

    @Nested
    class api_resource {
        @Test
        @WithAnonymousUser
        void can_be_accessed_anonymous() throws Exception {
            mockMvc.perform(get(anonymousHelloURI)
                            .queryParam("name", "jim"))
                    .andExpect(status().isOk())
                    .andExpect(MockMvcResultMatchers.content().string("hello, jim :caller=(null)"))
                    .andDo(MockMvcResultHandlers.print());
        }
        @Test
        @WithMockUser("user1")
        void can_be_accessed_authenticated() throws Exception {
            mockMvc.perform(get(authnHelloURI)
                            .queryParam("name", "jim"))
                    .andExpect(status().isOk())
                    .andExpect(content().string("hello, jim :caller=user1"))
                    .andDo(MockMvcResultHandlers.print());
        }
    }

    // using annotations -- bypassing credentials
    @Test
    @WithMockUser("user1")
    void local_authnmgr_builder_can_authn() throws Exception {
        mockMvc.perform(get(authnHelloURI)
                        .queryParam("name", "jim"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello, jim :caller=user1"));
    }

    @Test
    @WithMockUser("user2")
    void userdetails_service_can_authn() throws Exception {
        mockMvc.perform(get(authnHelloURI)
                        .queryParam("name", "jim"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello, jim :caller=user2"));
    }

    @Test
    @WithMockUser("user3")
    void jdbc_userdetails_can_authn() throws Exception {
        mockMvc.perform(get(authnHelloURI)
                        .queryParam("name", "jim"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello, jim :caller=user3"));
    }

    //using credentials
    @ParameterizedTest
    @MethodSource({"local_authnmgr_users", "userdetails_service_users", "jdbc_userdetails_users", "bad_credentials"})
    void user_can_authn(String username, String password, boolean valid) throws Exception {
        //given
        ResultActions request = mockMvc.perform(get(authnHelloURI)
                .with(httpBasic(username, password))
                .queryParam("name", "jim")
        );
        ResultMatcher expectedStatus = valid ? status().isOk() : status().isUnauthorized();
        String expectedContent = valid ? String.format("hello, jim :caller=%s", username) : "";
        //then
        request.andExpect(expectedStatus)
                .andExpect(content().string(expectedContent));
    }

    static Stream<Arguments> local_authnmgr_users() {
        return Stream.of(
                Arguments.of("user1", "password1", true),
                Arguments.of("user2", "password1", true)
        );
    }

    static Stream<Arguments> userdetails_service_users() {
        return Stream.of(
                Arguments.of("user1", "password2", true),
                Arguments.of("user3", "password2", true)
        );
    }

    static Stream<Arguments> jdbc_userdetails_users() {
        return Stream.of(
                Arguments.of("user1", "password", true),
                Arguments.of("user2", "password", true),
                Arguments.of("user3", "password", true)
        );
    }

    static Stream<Arguments> bad_credentials() {
        return Stream.of(
                Arguments.of("user1", "badpassword", false),
                Arguments.of("user4", "password1", false)
        );
    }
}
