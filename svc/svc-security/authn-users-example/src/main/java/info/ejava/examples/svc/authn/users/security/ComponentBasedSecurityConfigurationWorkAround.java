package info.ejava.examples.svc.authn.users.security;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import javax.sql.DataSource;
import java.util.List;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;
import static org.springframework.security.web.util.matcher.RegexRequestMatcher.regexMatcher;

//this issue appears to be fixed in Spring Boot 3.3.2 and can be deleted
//@Configuration(proxyBeanMethods = false)
public class ComponentBasedSecurityConfigurationWorkAround {
    /**
     * https://github.com/jzheaux/cve-2023-34035-mitigations
     * An explicit MvcRequestMatcher.Builder is necessary when mixing SpringMvc with
     * non-SpringMvc Servlets. Enabling the H2 console puts us in that position.
     * Dissabling (spring.h2.console.enabled=false) or being explicit as to which URI
     * apply to SpringMvc avoids the problem.
     * @param introspector
     * @return
     */
    @Bean
    MvcRequestMatcher.Builder mvc(HandlerMappingIntrospector introspector) {
        return new MvcRequestMatcher.Builder(introspector);
    }

    @Bean
    public WebSecurityCustomizer apiStaticResources(MvcRequestMatcher.Builder mvc) {
        return (web)->web.ignoring().requestMatchers(mvc.pattern("/content/**"));
    }

    @Bean
    @Order(0)
    public SecurityFilterChain apiSecurityFilterChain(HttpSecurity http, MvcRequestMatcher.Builder mvc) throws Exception {
        http.securityMatchers(m->m.requestMatchers(
                mvc.pattern("/api/anonymous/**"),
                mvc.pattern("/api/authn/**")
                ));
        http.authorizeHttpRequests(cfg->cfg.requestMatchers(
                mvc.pattern("/api/anonymous/**")).permitAll()
                );
        http.authorizeHttpRequests(cfg->cfg.anyRequest().authenticated());

        http.httpBasic(cfg->cfg.realmName("AuthConfigExample"));
        http.formLogin(cfg->cfg.disable());
        http.headers(cfg->{
            cfg.xssProtection(xss->xss.disable());
            cfg.frameOptions(fo->fo.disable());
        });
        http.csrf(cfg->cfg.disable());
        http.cors(cfg->cfg.configurationSource(corsLimitedConfigurationSource()));
        http.sessionManagement(cfg->cfg.sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        return http.build();
    }

    @Bean
    public AuthenticationManager authnManager(HttpSecurity http,
                                              List<UserDetailsService> userDetailsServices) throws Exception {
        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class);
        PasswordEncoder encoder = NoOpPasswordEncoder.getInstance();
        builder.inMemoryAuthentication().passwordEncoder(encoder)
                .withUser("user1").password(encoder.encode("password1")).roles()
                .and()
                .withUser("user2").password(encoder.encode("password1")).roles();
        for (UserDetailsService uds : userDetailsServices) {
            builder.userDetailsService(uds);
        }

        builder.parentAuthenticationManager(null); //prevent from being recursive
        return builder.build();
    }

    @Bean
    @Order(1000)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(cfg->cfg.anyRequest().denyAll());
        return http.build();
    }


    private CorsConfigurationSource corsLimitedConfigurationSource() {
        return (request) -> {
                CorsConfiguration config = new CorsConfiguration();
                //config.addAllowedOrigin("http://acme.com");
                config.addAllowedOrigin("http://localhost");
                config.setAllowedMethods(List.of("GET","POST"));
                return config;
        };
    }

    @Bean
    public CorsConfigurationSource corsPermitAllConfigurationSource() {
        return (request) -> {
            CorsConfiguration config = new CorsConfiguration();
            config.applyPermitDefaultValues();
            return config;
        };
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        //return NoOpPasswordEncoder.getInstance();
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public UserDetailsService sharedUserDetailsService(PasswordEncoder encoder) {
        //caution -- authorities broken in 3.1.0 / Spring 6.1.1, fixed in 6.1.2
        //same builder accumulates authorities between builds
        //https://github.com/spring-projects/spring-security/commit/3229bfa40ff6937c0bd75edad7e4a132533ce266
        User.UserBuilder ubuilder = User.builder().passwordEncoder(encoder::encode);
        List<UserDetails> users = List.of(
                ubuilder.username("user1").password("password2").roles().build(),
                ubuilder.username("user3").password("password2").roles().build()
        );

        return new InMemoryUserDetailsManager(users);
    }

    @Bean
    public UserDetailsService jdbcUserDetailsService(DataSource userDataSource) {
        //return new JdbcUserDetailsManager(userDataSource); -- for full CRUD
        //for just UserDetailsService query access
        JdbcDaoImpl jdbcUds = new JdbcDaoImpl();
        jdbcUds.setDataSource(userDataSource);
        return jdbcUds;
    }

    /**
     * Adding h2-console to application and protecting behind a FORM login fed off the
     * application's authentication manager.
     * @param http
     * @param authMgr
     */
    @Order(500)
    @Bean
    public SecurityFilterChain h2SecurityFilters(HttpSecurity http,
                     MvcRequestMatcher.Builder mvc, AuthenticationManager authMgr) throws Exception {
        http.securityMatchers(cfg->{cfg
                .requestMatchers( //h2-console not local SpringMVC application, must match URI
                    antMatcher("/h2-console*"),
                    antMatcher("/h2-console/**"))
                //can use MvcMatcher for form login pages
                .requestMatchers(
                    mvc.pattern("/login"),
                    mvc.pattern("/logout"),
                    mvc.pattern("/error"));
        });
        http.authorizeHttpRequests(cfg->cfg
                .requestMatchers(regexMatcher(HttpMethod.GET,".+(.css|.jsp|.gif)$")).permitAll()
                .anyRequest().authenticated()
        );
        http.formLogin(cfg->cfg
            .permitAll() //applies permitAll to standard login URIs
            .successForwardUrl("/h2-console")
        );
        http.csrf(cfg->cfg.ignoringRequestMatchers(antMatcher("/h2-console/**")));
        http.headers(cfg-> cfg.frameOptions(fo->fo.disable()));

        http.authenticationManager(authMgr);//reuse applications authz users
        return http.build();
    }
}
