package info.ejava.examples.svc.authn.users.controllers;

import info.ejava.examples.common.web.BaseExceptionAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice extends BaseExceptionAdvice {
}
