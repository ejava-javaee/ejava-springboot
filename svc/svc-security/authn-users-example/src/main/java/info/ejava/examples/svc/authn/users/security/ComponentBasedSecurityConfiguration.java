package info.ejava.examples.svc.authn.users.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.MediaTypeRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatchers;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.sql.DataSource;
import java.util.List;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;
import static org.springframework.security.web.util.matcher.RegexRequestMatcher.regexMatcher;

@Configuration(proxyBeanMethods = false)
public class ComponentBasedSecurityConfiguration {
    @Bean
    public WebSecurityCustomizer apiStaticResources() {
        return (web)->web.ignoring().requestMatchers("/content/**");
    }

    @Bean
    @Order(0)
    public SecurityFilterChain apiSecurityFilterChain(HttpSecurity http) throws Exception {
        http.securityMatchers(m->m.requestMatchers("/api/anonymous/**", "/api/authn/**"));
        http.authorizeHttpRequests(cfg->cfg.requestMatchers("/api/anonymous/**").permitAll());
        http.authorizeHttpRequests(cfg->cfg.anyRequest().authenticated());

        http.httpBasic(Customizer.withDefaults());
        http.formLogin(cfg->cfg.disable());
        http.headers(cfg->cfg.disable());
        http.csrf(cfg->cfg.disable());
        http.cors(cfg->cfg.configurationSource(corsLimitedConfigurationSource()));
        http.sessionManagement(cfg->cfg.sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        //handle noAuthn here with a 401, versus allowing an /error to be triggered
        http.exceptionHandling(cfg->cfg.defaultAuthenticationEntryPointFor(
                new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED),
                RequestMatchers.allOf()));

        return http.build();
    }

    @Bean
    public AuthenticationManager authnManager(HttpSecurity http,
                                              List<UserDetailsService> userDetailsServices) throws Exception {
        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class);
        PasswordEncoder encoder = NoOpPasswordEncoder.getInstance();
        builder.inMemoryAuthentication().passwordEncoder(encoder)
                .withUser("user1").password(encoder.encode("password1")).roles()
                .and()
                .withUser("user2").password(encoder.encode("password1")).roles();
        for (UserDetailsService uds : userDetailsServices) {
            builder.userDetailsService(uds);
        }

        builder.parentAuthenticationManager(null); //prevent from being recursive
         return builder.build();
    }

    @Bean
    @Order(1000)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(cfg->cfg.anyRequest().denyAll());
        return http.build();
    }


    private CorsConfigurationSource corsLimitedConfigurationSource() {
        return (request) -> {
                CorsConfiguration config = new CorsConfiguration();
                //config.addAllowedOrigin("http://acme.com");
                config.addAllowedOrigin("http://localhost");
                config.setAllowedMethods(List.of("GET","POST"));
                return config;
        };
    }

    @Bean
    public CorsConfigurationSource corsPermitAllConfigurationSource() {
        return (request) -> {
            CorsConfiguration config = new CorsConfiguration();
            config.applyPermitDefaultValues();
            return config;
        };
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        //return NoOpPasswordEncoder.getInstance();
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public UserDetailsService sharedUserDetailsService(PasswordEncoder encoder) {
        //caution -- authorities broken in 3.1.0 / Spring 6.1.1, fixed in 6.1.2
        //same builder accumulates authorities between builds
        //https://github.com/spring-projects/spring-security/commit/3229bfa40ff6937c0bd75edad7e4a132533ce266
        User.UserBuilder ubuilder = User.builder().passwordEncoder(encoder::encode);
        List<UserDetails> users = List.of(
                ubuilder.username("user1").password("password2").roles().build(),
                ubuilder.username("user3").password("password2").roles().build()
        );

        return new InMemoryUserDetailsManager(users);
    }

    @Bean
    public UserDetailsService jdbcUserDetailsService(DataSource userDataSource) {
        //return new JdbcUserDetailsManager(userDataSource); -- for full CRUD
        //for just UserDetailsService query access
        JdbcDaoImpl jdbcUds = new JdbcDaoImpl();
        jdbcUds.setDataSource(userDataSource);
        return jdbcUds;
    }

    /**
     * Adding h2-console to application and protecting behind a FORM login fed off the
     * application's authentication manager.
     * @param http
     * @param authMgr
     */
    @Order(500)
    @Bean
    public SecurityFilterChain h2SecurityFilters(HttpSecurity http, AuthenticationManager authMgr) throws Exception {
        MediaTypeRequestMatcher htmlRequestMatcher = new MediaTypeRequestMatcher(MediaType.TEXT_HTML);
        htmlRequestMatcher.setUseEquals(true);

        http.securityMatchers(cfg->cfg
                .requestMatchers("/h2-console*","/h2-console/**")
                .requestMatchers("/login", "/logout")
                .requestMatchers(RequestMatchers.allOf(
                        htmlRequestMatcher, //only want to service HTML error pages
                        AntPathRequestMatcher.antMatcher("/error")
                    ))
        );
        http.authorizeHttpRequests(cfg->cfg
                .requestMatchers(regexMatcher(HttpMethod.GET,".+(.css|.jsp|.gif)$")).permitAll()
                .anyRequest().authenticated()
        );
        http.formLogin(cfg->cfg
            .permitAll() //applies permitAll to standard login URIs
            .successForwardUrl("/h2-console")
        );
        http.csrf(cfg->cfg.ignoringRequestMatchers(antMatcher("/h2-console/**")));
        http.headers(cfg-> cfg.frameOptions(fo->fo.disable()));

        http.authenticationManager(authMgr);//reuse applications authz users
        return http.build();
    }
}
