package info.ejava.examples.svc.docker.votes;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;

@SpringBootConfiguration()
@EnableAutoConfiguration       //needed to setup logging
public class ClientTestConfiguration {
    @Bean @Lazy
    public URI baseVotesUrl(@LocalServerPort int port) throws URISyntaxException {
        return UriComponentsBuilder.fromUriString("http://localhost")
                .port(port)
                .path("/api/votes")
                .build().toUri();
    }
    @Bean @Lazy
    public URI baseElectionsUrl(@LocalServerPort int port) throws URISyntaxException {
        return UriComponentsBuilder.fromUriString("http://localhost")
                .port(port)
                .path("/api/elections")
                .build().toUri();
    }


    @Bean
    public RestTemplate anonymousUser(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        return restTemplate;
    }
}
