package info.ejava.examples.svc.docker.votes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import jakarta.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
@EnableAspectJAutoProxy
@Slf4j
public class VotesExampleApp {
	@Value("${spring.datasource.username:}")
	private String postgresUser;
	@Value("${spring.datasource.password:}")
	private String postgresPassword;

	public static void main(String[] args) {
		log.debug("{}", Arrays.asList(args));
		SpringApplication.run(VotesExampleApp.class, args);
	}

	@PostConstruct
	public void init() {
		log.info("postgres credentials={}/{}", postgresUser, postgresPassword);
	}

	@Bean
	@Order(Ordered.LOWEST_PRECEDENCE) //allows to be overridden using property
	public Jackson2ObjectMapperBuilderCustomizer jacksonCustomizer() {
		return builder -> builder.indentOutput(true)
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}

	@Bean
	public ObjectMapper jsonMapper(Jackson2ObjectMapperBuilder builder) {
		return builder.createXmlMapper(false).build();
	}
}
