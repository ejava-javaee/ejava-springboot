package info.ejava.examples.svc.aop.items;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import info.ejava.examples.svc.aop.items.dto.BedDTO;
import info.ejava.examples.svc.aop.items.introductions.MyUsageIntroduction;
import info.ejava.examples.svc.aop.items.proxyfactory.MyAccessIntroduction;
import info.ejava.examples.svc.aop.items.services.BedsServiceImpl;
import info.ejava.examples.svc.aop.items.services.ItemsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * Introductions add an extra interface and implementation to the advised bean.
 * This allows generic capability to coexist with the target.
 */
@SpringBootTest
@Slf4j
public class IntroductionAdviceTest {
    @Autowired
    private ItemsService<BedDTO> bedsService;

    @BeforeEach()
    void init() {
        //clear out any entries gathered prior to this test
        ((MyUsageIntroduction)bedsService).clear();
    }

    @Test
    void can_add_introduction_to_service() {
        //given
        //invoke method using target object and args
        BedDTO bed = BedDTO.bedBuilder().name("Single Bed").build();
        //when
        BedDTO createdBed = bedsService.createItem(bed); //Introduction advice triggered here
        log.info("created bed: {}", createdBed);

        //then
        then(createdBed.getId()).isNotZero();
        then(createdBed.getName()).isEqualTo(bed.getName());

        //access introduction for only advised method
        log.info("injected component implements interfaces: {}",
                ClassUtils.getAllInterfaces(bedsService.getClass()));
        then(ClassUtils.getAllInterfaces(bedsService.getClass())).contains(MyUsageIntroduction.class);

        //BedsService has a MyUsageIntroduction introduction and can be cast to that interface
        MyUsageIntroduction usage = (MyUsageIntroduction) bedsService;
        String signature = String.format("BedDTO %s.createItem(BedDTO):%s", BedsServiceImpl.class.getName(), bed);

        //inspect the result of the advice that was called before each advised method
        then(usage.getAllCalled()).containsExactly(signature);
    }

    @Test
    void can_add_introduction_to_service_response() throws JsonProcessingException {
        //given
        List<BedDTO> beds = IntStream.range(0,3)
                .mapToObj(i->BedDTO.bedBuilder().name("Bed" + i).build())
                .map(b->bedsService.createItem(b))
                .toList();
        //when
        List<BedDTO> retrievedBeds = beds.stream()
                .map(b->bedsService.getItem(b.getId())) //result from getItem will be advised
                .toList();

        //then
        //advised object implements target and introduction
        BedDTO retrievedBed = retrievedBeds.get(0);
        log.info("returned object interfaces: {}",
                ClassUtils.getAllInterfaces(retrievedBed.getClass()));
        then(ClassUtils.getAllInterfaces(retrievedBed.getClass())).contains(MyAccessIntroduction.class);
        then(retrievedBed).isInstanceOf(MyAccessIntroduction.class);
        MyAccessIntroduction<BedDTO> access = (MyAccessIntroduction<BedDTO>) retrievedBed;

        //each returned instance has independent set of roles assigned
        List<MyAccessIntroduction.Access> userRoles = retrievedBeds.stream()
                .map(b -> ((MyAccessIntroduction) b).getUserRoles()) //get roles from Introduction
                .flatMap(roles -> roles.stream()) //merge all roles collections into single stream
                .toList();
        then(userRoles).containsExactlyInAnyOrder(MyAccessIntroduction.Access.values());


        //when
        String toString = retrievedBed.toString();//toString is passed to target object by proxy
        String json = new ObjectMapper().writeValueAsString(retrievedBed);
        //then
        log.info("toString={}", toString);
        log.info("json={}", json); //json marshalled according to the JsonSerialize instruction
        then(toString).isEqualTo(String.format("BedDTO(super=ItemDTO(id=%d, name=%s))",
                retrievedBed.getId(),retrievedBed.getName()));
        then(json).startsWith("{").endsWith("}")
                .contains(String.format("\"userRoles\":[\"%s\"]", access.getUserRoles().get(0)),
                        String.format("\"id\":%d,\"name\":\"Bed0\"",retrievedBed.getId()));
    }
}
