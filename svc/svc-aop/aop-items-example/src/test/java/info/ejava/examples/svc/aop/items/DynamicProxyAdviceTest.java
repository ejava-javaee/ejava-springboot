package info.ejava.examples.svc.aop.items;

import info.ejava.examples.svc.aop.items.dynamicproxies.MyInvocationHandler;
import info.ejava.examples.svc.aop.items.dto.GrillDTO;
import info.ejava.examples.svc.aop.items.services.GrillsServiceImpl;
import info.ejava.examples.svc.aop.items.services.ItemsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import java.lang.reflect.Proxy;

import static org.assertj.core.api.BDDAssertions.then;

//@SpringBootTest
@Slf4j
@ExtendWith(OutputCaptureExtension.class)
public class DynamicProxyAdviceTest {
    //@Autowired
    //Instantiating a POJO instance to eliminate the Spring context polution that would be added to grillService
    //Functionally -- they both work -- but without the Spring context, you see only what the DynamicProxy is contributing
    //and you don't travel through any of the advice from the sibling AOP examples.
    private ItemsService<GrillDTO> grillService = new GrillsServiceImpl();

    <T> T given_a_dynamic_proxy_for(T target) {
        return (T) Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                ClassUtils.getAllInterfaces(target.getClass()).toArray(new Class[0]),
                new MyInvocationHandler(target));
    }

    @Test
    void can_dynamic_proxy_createItem(CapturedOutput capturedOutput) {
        //create instance of proxy using interface information and our custom invocation handler
        ItemsService<GrillDTO> grillsServiceProxy = given_a_dynamic_proxy_for(grillService);

        log.info("created proxy: {}", grillsServiceProxy.getClass());
        log.info("handler: {}", Proxy.getInvocationHandler(grillsServiceProxy).getClass());
        log.info("proxy implements interfaces: {}",
                ClassUtils.getAllInterfaces(grillsServiceProxy.getClass()));
        then(grillsServiceProxy.getClass().getSimpleName()).contains("$Proxy");
        then(Proxy.getInvocationHandler(grillsServiceProxy).getClass()).isEqualTo(MyInvocationHandler.class);
        then(ClassUtils.getAllInterfaces(grillsServiceProxy.getClass())).contains(ItemsService.class);

        //invoke the service using the proxy reference
        GrillDTO grill = GrillDTO.grillBuilder().name("Broil King").build();
        GrillDTO createdGrill = grillsServiceProxy.createItem(grill);
        log.info("created grill: {}", createdGrill);

        then(createdGrill.getId())
                .isNotZero()
                .isGreaterThan(grill.getId());
        then(createdGrill.getName()).isEqualTo(grill.getName());

        //look for proxy debug statements in logs
        String msg = String.format("invoke createItem returned: GrillDTO(super=ItemDTO(id=%d, name=Broil King))",createdGrill.getId());
        then(capturedOutput).contains("MyInvocationHandler",msg);
    }
}
