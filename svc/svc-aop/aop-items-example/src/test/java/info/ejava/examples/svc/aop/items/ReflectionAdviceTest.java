package info.ejava.examples.svc.aop.items;

import info.ejava.examples.svc.aop.items.dto.BedDTO;
import info.ejava.examples.svc.aop.items.dto.ItemDTO;
import info.ejava.examples.svc.aop.items.services.ItemsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(properties = {"enable.example.aspects=false"})
@Slf4j
public class ReflectionAdviceTest {
    @Autowired
    private ItemsService<BedDTO> bedsService;

    @Test
    void can_invoke_advised_method() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //obtain reference to method using name and argument types
        Method method = ItemsService.class.getMethod("createItem", ItemDTO.class);
        log.info("method: {}", method);

        //invoke method using target object and args
        BedDTO bed = BedDTO.bedBuilder().name("Bunk Bed").build();
        Object[] args = new Object[] { bed };
        log.info("invoke calling: {}({})", method.getName(), args);
        Object result = method.invoke(bedsService, args);
        log.info("invoke {} returned: {}", method.getName(), result);

        //obtain result from invoke() return
        BedDTO createdBed = (BedDTO) result;
        log.info("created bed: {}", createdBed);

        then(createdBed.getId()).isNotZero();
        then(createdBed.getName()).isEqualTo(bed.getName());
    }
}
