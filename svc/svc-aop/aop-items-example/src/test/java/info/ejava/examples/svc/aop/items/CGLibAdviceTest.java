package info.ejava.examples.svc.aop.items;

import info.ejava.examples.svc.aop.items.aspects.MyMethodInterceptor;
import info.ejava.examples.svc.aop.items.dto.ChairDTO;
import info.ejava.examples.svc.aop.items.services.ChairsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ClassUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.cglib.proxy.Enhancer;

import static org.assertj.core.api.BDDAssertions.then;

@Slf4j
@ExtendWith(OutputCaptureExtension.class)
public class CGLibAdviceTest {
    <T> T given_a_cglib_proxy(Class<T> target) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target);
        enhancer.setCallback(new MyMethodInterceptor());
        return (T)enhancer.create();
    }

    @Test
    void can_cglib_proxy_createItem(CapturedOutput capturedOutput) {
        //create an un-proxied, new instance of an extension of the target impl class
        //In this example, MyMethodInterceptor has nothing to delegate to except itself
        //If we had an existing instance to proxy, the design with have been slightly different
        ChairsServiceImpl chairsServiceProxy = given_a_cglib_proxy(ChairsServiceImpl.class);

        log.info("created proxy: {}", chairsServiceProxy.getClass());
        log.info("proxy implements interfaces: {}",
                ClassUtils.getAllInterfaces(chairsServiceProxy.getClass()));
        log.info("proxy superclasses: {}",
                ClassUtils.getAllSuperclasses(chairsServiceProxy.getClass()));
        then(chairsServiceProxy.getClass().getSimpleName()).contains("ChairsServiceImpl$$EnhancerByCGLIB$$");
        then(ClassUtils.getAllSuperclasses(chairsServiceProxy.getClass()))
                .contains(ChairsServiceImpl.class);

        ChairDTO chair = ChairDTO.chairBuilder().name("Recliner").build();
        ChairDTO createdChair = chairsServiceProxy.createItem(chair);

        log.info("created chair: {}", createdChair);
        then(createdChair.getId()).isNotZero();
        then(createdChair.getName()).isEqualTo(chair.getName());

        //look for proxy debug statements in logs
        String msg = String.format("invoke createItem returned: ChairDTO(super=ItemDTO(id=%d, name=%s))",
                createdChair.getId(), createdChair.getName());
        then(capturedOutput).contains("MyMethodInterceptor",msg);
    }
}
