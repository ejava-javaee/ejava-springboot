package info.ejava.examples.svc.aop.items;

import info.ejava.examples.common.exceptions.ClientErrorException;
import info.ejava.examples.svc.aop.items.dto.MowerDTO;
import info.ejava.examples.svc.aop.items.services.MowersServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Slf4j
public class AOPAdviceDemoTest {
    @Autowired
    MowersServiceImpl mowersService;
    MowerDTO mower;

    @BeforeEach
    void init() {
        log.info("starting AOP advice demo");
        mower = mowersService.createItem(MowerDTO.mowerBuilder().name("bush hog").build());
    }

    @AfterEach
    void tearDown() {
        mowersService.deleteItems();
    }

    @Test
    void mower_update_successful() {
        mower = mowersService.updateItem(mower.getId(), MowerDTO.mowerBuilder().name("bush hog").build());
        log.info("mower created: {}", mower);
    }

    @Test
    void mower_update_failure() {
        ClientErrorException ex = assertThrows(ClientErrorException.NotFoundException.class, () ->
                mowersService.updateItem(13, MowerDTO.mowerBuilder().name("does not exist").build())
        );
        log.info("mower exception: {}", ex.getError());
    }
}
