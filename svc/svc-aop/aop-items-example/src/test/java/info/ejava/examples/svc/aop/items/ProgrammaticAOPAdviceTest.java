package info.ejava.examples.svc.aop.items;

import info.ejava.examples.svc.aop.items.proxyfactory.SampleAdvice1;
import info.ejava.examples.svc.aop.items.dto.MowerDTO;
import info.ejava.examples.svc.aop.items.services.ItemsService;
import info.ejava.examples.svc.aop.items.services.MowersServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

import static org.assertj.core.api.BDDAssertions.then;

@ExtendWith(OutputCaptureExtension.class)
public class ProgrammaticAOPAdviceTest {
    ItemsService<MowerDTO> mowerService = new MowersServiceImpl();

    @Test
    void can_invoke_without_advice(CapturedOutput capturedOutput) {
        //given
        MowerDTO mower = MowerDTO.mowerBuilder().name("John Deer").build();
        //when
        MowerDTO createdMower = mowerService.createItem(mower);
        //then
        then(createdMower.getId()).isNotZero();
        then(createdMower.getName()).isEqualTo(mower.getName());

        //look for proxy debug statements in logs
        then(capturedOutput).doesNotContain("SampleAdvice1","before:","after:");
    }

    <T> T given_advice(T target) {
        SampleAdvice1 advice1 = new SampleAdvice1();
        ProxyFactory proxyFactory = new ProxyFactory(target);
        proxyFactory.addAdvice(advice1);
        return (T) proxyFactory.getProxy();
    }

    @Test
    void can_advise_createItem(CapturedOutput capturedOutput) {
        //given
        MowerDTO mower = MowerDTO.mowerBuilder().name("Husqvarna").build();
        ItemsService<MowerDTO> proxiedMowerService = given_advice(mowerService);

        //when
        MowerDTO createdMower = proxiedMowerService.createItem(mower);
        //then
        then(createdMower.getId()).isNotZero();
        then(createdMower.getName()).isEqualTo(mower.getName());

        //look for proxy debug statements in logs
        then(capturedOutput).contains("SampleAdvice1","before:","after:");
    }
}
