package info.ejava.examples.svc.rpc.greeter;

import info.ejava.examples.svc.rpc.GreeterApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.Charset;

import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;

/**
 * This class provides a simple example of using RestClient as a
 * fluent API replacement for RestTemplate.
 */
@SpringBootTest(classes = {GreeterApplication.class, //optionally naming app config
        ClientTestConfiguration.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("springboot") @Tag("greeter")
@Slf4j
public class GreeterRestClientNTest {
    @LocalServerPort
    private int port; //injection option way1
    String baseUrl; //initialized in test setup
    private RestClient restClient; //initialized in test setup

    @Autowired @Qualifier("baseUrl") //qualifier makes bean selection more explicit
    private String injectedBaseUrl;  //initialized in test config using way3
    @Autowired
    private RestClient injectedRestClient; //injected from test config

    @BeforeEach
    public void init(@LocalServerPort int serverPort) { //injection option way2
        baseUrl = String.format("http://localhost:%d/rpc/greeter", serverPort);
        restClient = RestClient.builder().build();
    }


    @Test
    public void say_hi() {
        //given - a service available at a URL and client access
        String url = String.format("http://localhost:%d/rpc/greeter/sayHi", port);
        RestClient restClient = RestClient.builder().build();

        //when - calling the service
        String greeting = restClient.get()
                .uri(url)
                .retrieve()
                .body(String.class);

        //then - we get a greeting response
        then(greeting).isEqualTo("hi");
    }

    @Test
    public void say_greeting() {
        //given - a service available to provide a greeting
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/say/{greeting}")
                .queryParam("name", "{name}")
                .build("hello", "jim");

        //when - asking for that greeting
        ResponseEntity<String> response = restClient.get()
                .uri(url)
                .retrieve()
                .toEntity(String.class);

        //then - response be successful with expected greeting
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).startsWith("text/plain");
        then(response.getBody()).isEqualTo("hello, jim");
    }

    @Test
    public void no_boom() {
        //given - a URL with all required properties
        URI url = UriComponentsBuilder.fromHttpUrl(injectedBaseUrl) //using injected base URL
                .replacePath("/rpc/greeter/boom")
                .queryParam("value", "whatever")
                .build().toUri();

        //when - calling the service -- using injected WebClient
        ResponseEntity<String> response = injectedRestClient.get()
            .uri(url)
            .retrieve()
            .toEntity(String.class);

        //then - no error
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody()).isEqualTo("worked?");
    }

    @Test
    public void boom() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .replacePath("/rpc/greeter/boom")
                .build().toUri();
        //when - calling the service
        HttpClientErrorException ex = catchThrowableOfType(
                () -> restClient.get().uri(url).retrieve().toEntity(String.class),
                HttpClientErrorException.BadRequest.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
        log.info("{}", ex.getResponseBodyAsString());
    }

    @Test
    public void boom_return_response_not_exception() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .replacePath("/rpc/greeter/boom")
                .build().toUri();
        //when - calling the service
        Assertions.assertDoesNotThrow(() -> {
            ResponseEntity<?> response = restClient.get().uri(url)
                    .exchange((req, resp) -> {
                        return ResponseEntity.status(resp.getStatusCode())
                                .headers(resp.getHeaders())
                                .body(StreamUtils.copyToString(resp.getBody(), Charset.defaultCharset()));
                    });
            //then - we get a bad request
            then(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
            log.info("{}", response.getBody());
        });
    }

    @Test
    public void boy() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys")
                .queryParam("name", "jim")
                .build().toUri();
        //when - calling the service
        ResponseEntity<String> response = restClient.get()
                .uri(url)
                .retrieve()
                .toEntity(String.class);

        //then - we get a valid response
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isEqualTo(url.toString());
        then(response.getBody()).isEqualTo("hello jim, how do you do?");
    }

    @Test
    public void boy_blue() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys")
                .queryParam("name", "blue")
                .build().toUri();
        //when - calling the service
        RestClientResponseException ex = catchThrowableOfType(
                () -> restClient.get().uri(url).retrieve().toEntity(String.class),
                HttpClientErrorException.UnprocessableEntity.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isNull();
        then(ex.getResponseBodyAsString()).isEqualTo("boy named blue?");
    }

    @Test
    public void boy_blue_with_exception_handler() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys/throws") //<== new URI
                .queryParam("name", "blue")
                .build().toUri();
        //when - calling the service
        RestClientResponseException ex = catchThrowableOfType(
                () -> restClient.get().uri(url).retrieve().toEntity(String.class),
                HttpClientErrorException.UnprocessableEntity.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isNull();
        then(ex.getResponseBodyAsString()).isEqualTo("boy named blue?");
    }
}
