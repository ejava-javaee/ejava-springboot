package info.ejava.examples.svc.rpc.greeter;

import info.ejava.examples.svc.rpc.GreeterApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;

/**
 * This class provides a simple example of using RestClient as a
 * fluent API replacement for RestTemplate.
 */
@SpringBootTest(classes = {GreeterApplication.class, //optionally naming app config
        ClientTestConfiguration.class},
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("springboot") @Tag("greeter")
@Slf4j
public class GreeterHttpIfaceNTest {
    @LocalServerPort
    private int port; //injection option way1
    String baseUrl; //initialized in test setup
    private GreeterAPI greeterAPI; //initialized in test setup

    @Autowired @Qualifier("baseUrl") //qualifier makes bean selection more explicit
    private String injectedBaseUrl;  //initialized in test config using way3
    @Autowired
    private GreeterAPI injectedHttpIface; //injected from test config


    @BeforeEach
    public void init(@LocalServerPort int serverPort) { //injection option way2
        baseUrl = String.format("http://localhost:%d/rpc/greeter", serverPort);
        RestClient restClient = RestClient.builder()
                .baseUrl(baseUrl)
                .build();

        RestClientAdapter adapter = RestClientAdapter.create(restClient);
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();
        greeterAPI = factory.createClient(GreeterAPI.class);
    }


    @Test
    public void say_hi() {
        //given - a service available at a URL and client access
        String url = String.format("http://localhost:%d/rpc/greeter", port);
        RestClient restClient = RestClient.builder().baseUrl(url).build();

        interface MyGreeter {
            @GetExchange("/sayHi")
            String sayHi();
        }

        RestClientAdapter adapter = RestClientAdapter.create(restClient);
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();
        MyGreeter greeterAPI = factory.createClient(MyGreeter.class);

        //when - calling the service
        String greeting = greeterAPI.sayHi();

        //then - we get a greeting response
        then(greeting).isEqualTo("hi");
    }

    @Test
    public void say_greeting() {
        //when - asking for that greeting
        ResponseEntity<String> response = greeterAPI.sayGreeting("hello","jim");

        //then - response be successful with expected greeting
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).startsWith("text/plain");
        then(response.getBody()).isEqualTo("hello, jim");
    }

    @Test
    public void say_greeting_with_default_param() {
        //when - asking for that greeting
        ResponseEntity<String> response = greeterAPI.sayGreeting("hello",null);

        //then - response be successful with expected greeting
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).startsWith("text/plain");
        then(response.getBody()).isEqualTo("hello, client");
    }

    @Test
    public void no_boom() {
        //when - calling the service -- using injected Http Interface
        ResponseEntity<String> response = injectedHttpIface.boom("whatever");
        //then - no error
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody()).isEqualTo("worked?");
    }

    @Test
    public void boom() {
        //when - calling the service
        RestClientResponseException ex = catchThrowableOfType(
                () -> greeterAPI.boom(),
                HttpClientErrorException.BadRequest.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
        log.info("{}", ex.getResponseBodyAsString());
    }

    @Test
    public void boy() {
        //when - calling the service
        ResponseEntity<String> response =  greeterAPI.createBoy("jim");

        //then - we get a valid response
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_LOCATION)).isNotEmpty();
        then(response.getBody()).isEqualTo("hello jim, how do you do?");
    }

    @Test
    public void boy_blue() {
        //when - calling the service with bad name
        RestClientResponseException ex = catchThrowableOfType(
                () -> greeterAPI.createBoy("blue"),
                HttpClientErrorException.UnprocessableEntity.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isNull();
        then(ex.getResponseBodyAsString()).isEqualTo("boy named blue?");
    }
}
