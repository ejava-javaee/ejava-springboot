package info.ejava.examples.svc.rpc.greeter;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@HttpExchange(accept = { MediaType.TEXT_PLAIN_VALUE, //this example primarily works with Strings
        MediaType.APPLICATION_JSON_VALUE })  //by allowing JSON, we will get error payloads
interface GreeterAPI {
    @GetExchange("/sayHi")
    String sayHi();
    @GetExchange("/say/{greeting}")
    ResponseEntity<String> sayGreeting(@PathVariable(value = "greeting", required = true) String greeting,
                                       @RequestParam(value = "name", defaultValue = "client") String name);
    @GetExchange("/boom") //this one will work when given a value
    ResponseEntity<String> boom(@RequestParam(value = "value", required = true) String value);
    @GetExchange("/boom") //this one will fail; defined separately to allow no value
    ResponseEntity<String> boom();

    @GetExchange("/boys")
    ResponseEntity<String> createBoy(@RequestParam("name") String name);
}
