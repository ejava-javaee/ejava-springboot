package info.ejava.examples.svc.rpc.greeter;

import info.ejava.examples.svc.rpc.GreeterApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.NoOpResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;

/**
 * This class provides a simple example of using the synchronous
 * RestTemplate client.
 */
@SpringBootTest(classes = {GreeterApplication.class, //optionally naming app config
        ClientTestConfiguration.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("springboot") @Tag("greeter")
@Slf4j
public class GreeterRestTemplateNTest {
    @LocalServerPort
    //@Value("${local.server.port}") - alternative
    private int port; //injection option way1
    private String baseUrl; //initialized in test setup using way1 or way2
    private RestTemplate restTemplate; //initialized in test setup

    @Autowired @Qualifier("baseUrl") //qualifier makes bean selection more explicit
    private String injectedBaseUrl; //initialized in test config using way3
    @Autowired
    private RestTemplate injectedRestTemplate; //injected from test config

    @BeforeEach
    public void init(@LocalServerPort int serverPort) { //injection option way2
        baseUrl = String.format("http://localhost:%d/rpc/greeter", serverPort);
        restTemplate = new RestTemplate();
    }

    @Test
    public void say_hi() {
        //given - a service available at a URL and client access
        String url = String.format("http://localhost:%d/rpc/greeter/sayHi", port);
//more type-safe, purpose-designed way to build the URL
//        URI url = UriComponentsBuilder.fromHttpUrl("http://localhost")
//                .port(port)
//                .path("rpc/greeter/sayHi")
//                .build().toUri();
        RestTemplate restTemplate = new RestTemplate(); //simple, manual instantiation

        //when - calling the service
        String greeting = restTemplate.getForObject(url, String.class);

        //then - we get a greeting response
        then(greeting).isEqualTo("hi");
    }

    @Test
    public void say_greeting() {
        //given - a service available to provide a greeting
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/say/{greeting}")
                .queryParam("name", "{name}")
                .build("hello", "jim");

        //when - asking for that greeting -- using RestTemplate from test setup
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        //then - response be successful with expected greeting
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).startsWith("text/plain");
        then(response.getBody()).isEqualTo("hello, jim");
    }

    @Test
    public void no_boom() {
        //given - a URL with all required properties
        URI url = UriComponentsBuilder.fromHttpUrl(injectedBaseUrl) //using injected base URL
                .replacePath("/rpc/greeter/boom")
                .queryParam("value", "whatever")
                .build().toUri();
        //when - calling the service -- using injected RestTemplate
        ResponseEntity<String> response = injectedRestTemplate.getForEntity(url, String.class);

        //then - no error
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody()).isEqualTo("worked?");
    }

    @Test
    public void boom() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .replacePath("/rpc/greeter/boom")
                .build().toUri();
        //when - calling the service
        HttpClientErrorException ex = catchThrowableOfType(
                ()->restTemplate.getForEntity(url, String.class),
                HttpClientErrorException.BadRequest.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
        log.info("{}", ex.getResponseBodyAsString());
    }

    @Test
    public void boom_return_response_not_exception() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .replacePath("/rpc/greeter/boom")
                .build().toUri();
        //configure RestTemplate to return error responses, not exceptions
        RestTemplate noExceptionRestTemplate = new RestTemplate();
        noExceptionRestTemplate.setErrorHandler(new NoOpResponseErrorHandler());

        //when - calling the service
        Assertions.assertDoesNotThrow(()->{
            ResponseEntity<String> response = noExceptionRestTemplate.getForEntity(url, String.class);
            //then - we get a bad request
            then(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            then(response.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo(MediaType.APPLICATION_JSON_VALUE);
            log.info("{}", response.getBody());
        },"return response, not exception");
    }


    @Test
    public void boy() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys")
                .queryParam("name", "jim")
                .build().toUri();
        //when - calling the service
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        //then - we get a bad request
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isEqualTo(url.toString());
        then(response.getBody()).isEqualTo("hello jim, how do you do?");
    }

    @Test
    public void boy_blue() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys")
                .queryParam("name", "blue")
                .build().toUri();
        //when - calling the service
        HttpClientErrorException ex = catchThrowableOfType(
                ()->restTemplate.getForEntity(url, String.class),
                HttpClientErrorException.UnprocessableEntity.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isNull();
        then(ex.getResponseBodyAsString()).isEqualTo("boy named blue?");
    }

    @Test
    public void boy_blue_with_exception_handler() {
        //given - a URL with a missing required query param
        URI url = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path("/boys/throws") //<== new URI
                .queryParam("name", "blue")
                .build().toUri();
        //when - calling the service
        HttpClientErrorException ex = catchThrowableOfType(
                ()->restTemplate.getForEntity(url, String.class),
                HttpClientErrorException.UnprocessableEntity.class);

        //then - we get a bad request
        then(ex.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
        then(ex.getResponseHeaders().getFirst(HttpHeaders.CONTENT_LOCATION))
                .isNull();
        then(ex.getResponseBodyAsString()).isEqualTo("boy named blue?");
    }
}
