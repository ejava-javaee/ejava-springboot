package info.ejava.examples.svc.rpc.greeter;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@TestConfiguration(proxyBeanMethods = false)
public class ClientTestConfiguration {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
        //or just the following will work in the simple cases like this
        //return new RestTemplate()
    }
    @Bean
    public WebClient webClient(WebClient.Builder builder) {
        return builder.build();
        //or just the following will work in the simple cases like this
        //return WebClient.builder().build();
    }
    @Bean
    public RestClient restClient(RestClient.Builder builder, RestTemplate restTemplate) {
        //or just the following will work in the simple cases like this
        //return RestClient.builder().build();
        return builder.build();
    }
    @Bean @Lazy
    //this could be done with RestTemplate, RestClient, or RestClient
    public GreeterAPI greeterAPI(RestClient.Builder builder, String baseUrl) {
        RestClient client = builder.baseUrl(baseUrl).build();
        RestClientAdapter adapter = RestClientAdapter.create(client);
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();
        return factory.createClient(GreeterAPI.class);
    }

    @Bean @Lazy
    public String baseUrl(@LocalServerPort int serverPort) { //injection option way3
        return String.format("http://localhost:%d/rpc/greeter", serverPort);
    }
}
