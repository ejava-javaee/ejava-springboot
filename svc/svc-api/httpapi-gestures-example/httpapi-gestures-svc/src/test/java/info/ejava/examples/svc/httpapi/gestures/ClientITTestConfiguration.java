package info.ejava.examples.svc.httpapi.gestures;

import info.ejava.examples.svc.httpapi.gestures.api.GesturesAPI;
import info.ejava.examples.svc.httpapi.gestures.client.GesturesAPISyncWebClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * A test configuration used by remote IT test clients.
 */
@TestConfiguration
@EnableAutoConfiguration
@EnableConfigurationProperties //used to set it.server properties
public class ClientITTestConfiguration extends ClientTestBaseConfiguration {
    @Bean
    @ConfigurationProperties("it.server")
    public ServerConfig itServerConfig() {
        return new ServerConfig();
    }

    @Bean
    @Qualifier("webclient")
    public GesturesAPI gesturesWebClient(WebClient webClient, ServerConfig cfg) {
        return new GesturesAPISyncWebClient(webClient, cfg);
    }
}
