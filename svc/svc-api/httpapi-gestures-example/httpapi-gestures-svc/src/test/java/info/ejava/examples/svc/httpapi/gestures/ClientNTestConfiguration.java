package info.ejava.examples.svc.httpapi.gestures;

import info.ejava.examples.svc.httpapi.gestures.api.GesturesAPI;
import info.ejava.examples.svc.httpapi.gestures.client.GesturesAPIRestClient;
import info.ejava.examples.svc.httpapi.gestures.client.GesturesAPISyncWebClient;
import info.ejava.examples.svc.httpapi.gestures.client.GesturesAPITemplateClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@TestConfiguration
public class ClientNTestConfiguration extends ClientTestBaseConfiguration {

    @Bean @Lazy
    public ServerConfig serverConfig(@LocalServerPort int port) {
        return new ServerConfig().withPort(port).build();
    }

    @Bean @Lazy
    @Primary
    public GesturesAPI gesturesTemplateClient(RestTemplate restTemplate, ServerConfig cfg) {
        return new GesturesAPITemplateClient(restTemplate, cfg);
    }

    @Bean @Lazy
    @Qualifier("restclient")
    public GesturesAPI gesturesRestClient(RestClient restClient, ServerConfig serverConfig) {
        return new GesturesAPIRestClient(restClient, serverConfig);
    }

    @Bean @Lazy
    @Qualifier("webclient")
    public GesturesAPI gesturesWebClient(WebClient webClient, ServerConfig cfg) {
        return new GesturesAPISyncWebClient(webClient, cfg);
    }



}
