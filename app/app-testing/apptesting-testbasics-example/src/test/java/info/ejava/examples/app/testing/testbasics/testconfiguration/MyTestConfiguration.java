package info.ejava.examples.app.testing.testbasics.testconfiguration;

import info.ejava.examples.app.testing.testbasics.tips.ServiceQuality;
import info.ejava.examples.app.testing.testbasics.tips.TipCalculator;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

//@Configuration //picked up in standard component scan
@TestConfiguration(proxyBeanMethods = false) //skipped in component scan -- must be manually identified
public class MyTestConfiguration {
    @Bean
    public TipCalculator standardTippingImpl() {
        return new TipCalculator() {
            @Override
            public BigDecimal calcTip(BigDecimal amount, ServiceQuality serviceQuality) {
                return BigDecimal.ZERO;
            }
        };
    }
}

