package info.ejava.examples.app.build.javamain;

import java.util.List;

public class SimpleMainApp {
    public static void main(String... args) {
        System.out.println("Hello " + List.of(args));
    }
}
