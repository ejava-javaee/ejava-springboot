FROM maven:3.8.6-eclipse-temurin-17-focal

# https://docs.docker.com/engine/install/ubuntu/
# Add Docker's official GPG key:
RUN apt-get update && \
 apt-get install -y ca-certificates curl && \
 install -m 0755 -d /etc/apt/keyrings && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc && \
 chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
RUN echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null && \
  apt-get update


#install both docker CLI and docker-compose
RUN apt-get update && \
    apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin rsync graphviz


#grab the "docker-compose" wrapper script to make Testcontainers happy
RUN curl -L $(echo "https://github.com/docker/compose/releases/download/v2.28.1/docker-compose-$(uname -s)-$(uname -m)" \
  | tr '[:upper:]' '[:lower:]') \
  -o /usr/local/bin/docker-compose && \
	chmod go+x /usr/local/bin/docker-compose


#allow image to access home directory when not running as root
RUN chmod 777 /root

#make ssh happy
RUN groupadd -g 1000 jenkins
RUN useradd -u 1000 -g 1000 jenkins -d /root
