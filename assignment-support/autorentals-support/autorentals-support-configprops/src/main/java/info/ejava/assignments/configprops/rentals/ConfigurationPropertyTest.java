package info.ejava.assignments.configprops.rentals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This test does a light verification of the populated PropertyPrinter component.
 * It uses reflection because all of the data types are in the starter project
 * and it is unknown what Java packaging things will use.
 *
 * We will cover testing soon and reflection several weeks after that
 */
@SpringBootTest
public class ConfigurationPropertyTest {
    private Object component;

    @BeforeEach
    void init(@Autowired ApplicationContext ctx) throws Exception {
        component = ctx.getBean("propertyPrinter");
    }

    @Test
    void component_has_autorentals() throws Exception {
        List<?> autos = get(component, "autos", List.class);
        assertThat(autos).as("autos not injected").isNotNull();
        assertThat(autos).as("autos").hasSize(3);
        assertThat(autos)
                .extracting(object->getId(object))
                .as("autoId")
                .containsAll(List.of(1,4,5));

        Object rental = autos.stream()
                .filter(s->((Integer)getId(s)).equals(1))
                .findFirst()
                .orElseThrow();

        assertThat(get(rental, "renterName", String.class)).as(()->context(rental)+".renterName").isEqualTo("Joe Camper");
        assertThat(get(rental, "rentalDate", LocalDate.class)).as(()->context(rental)+".rentalDate").isEqualTo(LocalDate.of(2010,7,1));
        assertThat(get(rental, "rentalAmount", BigDecimal.class)).as(()->context(rental)+".rentalAmount").isEqualTo(BigDecimal.valueOf(100.00));
        Object location = get(rental, "location", Object.class);
        assertThat(location).as(()->context(rental)+".location").isNotNull();
        assertThat(get(location, "city", String.class)).as(()->context(rental) + ".location.city").isNotNull();
        assertThat(get(location, "state", String.class)).as(()->context(rental)+".location.state").isNotNull();
    }

    private String context(Object rental) {
        return String.format("rental[%d]", getId(rental));
    }

    @Test
    void component_has_toolrentals() throws Exception {
        List<?> tools = get(component,"tools", List.class);
        assertThat(tools).as("tools not injected").isNotNull();
        assertThat(tools).as("tools").hasSize(1);
        assertThat(tools)
                .extracting(object->getId(object))
                .as("autoId")
                .containsAll(List.of(2));
    }
    @Test
    void component_has_boatrental() throws Exception {
        Object boat = get(component,"boat", Object.class);
        assertThat(boat).as("boat is not injected").isNotNull();
        assertThat(getId(boat)).as("boatId").isEqualTo(3);
    }


    private int getId(Object obj) {
        return get(obj, "id", Integer.class);
    }
    private <T> T get(Object obj, String property, Class<T> type) {
        return (T) ReflectionTestUtils.invokeGetterMethod(obj, property);
//        String getter = "get" + StringUtils.capitalize(property);
//        try {
//            Method getId = obj.getClass().getDeclaredMethod(getter);
//            return (T) getId.invoke(obj);
//        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException ex) {
//            throw new RuntimeException(ex);
//        }
    }

}
