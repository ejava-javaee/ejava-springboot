package info.ejava.assignments.security.autorenters.svc.rentals;

import info.ejava.assignments.api.autorenters.client.autos.AutosAPI;
import info.ejava.assignments.api.autorenters.client.autos.AutosAPIClient;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPIClient;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import info.ejava.examples.common.web.ServerConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssumptions.given;


//@SpringBootTest(classes= { ...
//    },
//    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles({"test","userdetails"})
//@DisplayName("Part A3: User Details")
@Slf4j
public class A3_UserDetailsNTest {
    @Autowired
    @Qualifier("usernameMap")
    private Map<String, RestTemplate> authnUsers;
    @Autowired
    private RestTemplate anonymousUser;
    @Autowired
    AutoDTOFactory autoFactory;
    @Autowired
    RenterDTOFactory renterFactory;
    @Autowired
    AutosAPIClient autosAPIClient;
    @Autowired
    RentersAPIClient rentersAPIClient;
    @Autowired
    ApiTestHelper<RentalDTO> testHelper;
    @Autowired
    private ServerConfig serverConfig;
    private URI whoAmIUrl;
    private @Autowired Environment env;
    @Autowired(required = false)
    List<PasswordEncoder> passwordEncoder;
    @Autowired(required = false)
    List<UserDetailsService> userDetails;

    @BeforeEach
    void init() {
        //Assumptions.assumeFalse(getClass().equals(A3_UserDetailsNTest.class),"should only run for derived class");
        given(userDetails).as("no user details found for accounts").isNotEmpty();
        given(passwordEncoder).as("no password encoder found").isNotEmpty();
        given(env.getActiveProfiles()).as("missing required profile").contains("userdetails");

        whoAmIUrl = UriComponentsBuilder.fromUri(serverConfig.getBaseUrl())
                .path("api/whoAmI")
                .build().toUri();
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class with_identities {

        @ParameterizedTest(name = "{index} {1} can authenticate")
        @MethodSource("identities")

        void valid_credentials_can_authenticate(RestTemplate authnUser, String username) {
            //given
            RequestEntity<Void> request = RequestEntity.get(whoAmIUrl).build();
            //when
            ResponseEntity<String> response = authnUser.exchange(request, String.class);
            String body = response.getBody();
            //then
            log.info("user={}", body);
            then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            then(body).isEqualTo(username);
        }

        @ParameterizedTest(name = "{index} {1} can create auto")
        @MethodSource("identities")
        void valid_credentials_can_create_auto(RestTemplate authnUser, String username) {
            //given
            AutoDTO validAuto = autoFactory.make();
            AutosAPI autosClient = autosAPIClient.withRestTemplate(authnUser);
            //when
            ResponseEntity<AutoDTO> response = autosClient.createAuto(validAuto);
            //then
            then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        }

        Stream<Arguments> identities() {
            return authnUsers.entrySet().stream()
                        .map(au->Arguments.of(au.getValue(), au.getKey()));
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class create_renter {
        @BeforeEach
        void cleanUp() {
            //we can only have 1 renter per username -- cleanup
            RestTemplate authnUser = authnUsers.values().iterator().next();
            rentersAPIClient.withRestTemplate(authnUser).removeAllRenters();
        }


        @ParameterizedTest(name = "{index} {1} can create renter")
        @MethodSource("identities")
        void valid_credentials_can_create_renter(RestTemplate authnUser, String username) {
            //given
            RenterDTO validRenter = renterFactory.make();
            RentersAPI rentersClient = rentersAPIClient.withRestTemplate(authnUser);
            //when
            ResponseEntity<RenterDTO> response = rentersClient.createRenter(validRenter);
            //then
            then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        }

        Stream<Arguments> identities() {
            return authnUsers.entrySet().stream()
                        .map(au->Arguments.of(au.getValue(), au.getKey()));
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class create_auto {

        @ParameterizedTest(name = "{index} {1} can create auto")
        @MethodSource("identities")
        void valid_credentials_can_create_auto(RestTemplate authnUser, String username) {
            //given
            AutoDTO validAuto = autoFactory.make();
            AutosAPIClient autosClient = autosAPIClient.withRestTemplate(authnUser);
            //when
            ResponseEntity<AutoDTO> response = autosClient.createAuto(validAuto);
            //then
            then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        }

        Stream<Arguments> identities() {
            return authnUsers.entrySet().stream()
                    .map(au->Arguments.of(au.getValue(), au.getKey()));
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class create_rental {
        @AfterAll
        void cleanUp() {
            //we can only have 1 renter per username -- cleanup
            RestTemplate authnUser = authnUsers.values().iterator().next();
            try {
                testHelper.withRestTemplate(authnUser).removeAllRentals();
            } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.Unauthorized ex) {
                fail("authenticated user failed to delete all in profile that does not have roles; " +
                        "if this worked eariler, make sure you have applied authorization checks in a way that " +
                        "will not be active during profile(s): " + Arrays.toString(env.getActiveProfiles()));
            }
            rentersAPIClient.withRestTemplate(authnUser).removeAllRenters();
            autosAPIClient.withRestTemplate(authnUser).removeAllAutos();
        }

        @ParameterizedTest(name = "{index} {1} can create contract")
        @MethodSource("identities")
        void valid_credentials_can_create_contract(RestTemplate authnUser, String username) {
            //given
            AutosAPI autosClient = autosAPIClient.withRestTemplate(authnUser);
            RentersAPI rentersClient = rentersAPIClient.withRestTemplate(authnUser);
            ApiTestHelper<RentalDTO> rentalsHelper = testHelper.withRestTemplate(authnUser);

            AutoDTO auto = autosClient.createAuto(autoFactory.make()).getBody();
            RenterDTO renter = rentersClient.createRenter(renterFactory.make()).getBody();
            TimePeriod timePeriod = new TimePeriod(LocalDate.now(),2);
            RentalDTO proposal = rentalsHelper.makeProposal(auto, renter, timePeriod);
            //when
            ResponseEntity<RentalDTO> response = rentalsHelper.createContract(proposal);
            //then
            then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        }

        Stream<Arguments> identities() {
            return authnUsers.entrySet().stream()
                        .map(au->Arguments.of(au.getValue(), au.getKey()));
        }
    }
}
