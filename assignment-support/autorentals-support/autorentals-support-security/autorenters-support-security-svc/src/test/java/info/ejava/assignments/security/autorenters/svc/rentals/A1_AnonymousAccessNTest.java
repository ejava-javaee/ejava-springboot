package info.ejava.assignments.security.autorenters.svc.rentals;

import info.ejava.assignments.api.autorenters.client.autos.AutosAPI;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.autos.AutoListDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.SearchParams;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import info.ejava.examples.common.web.ServerConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.BDDAssertions.fail;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.junit.jupiter.api.Assertions.assertThrows;

//@SpringBootTest(classes= { ...
//    },
//    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles({"test", "anonymous-access"})
//@DisplayName("Part A1: Anonymous Access")
@Slf4j
public class A1_AnonymousAccessNTest {
    @Autowired
    private ApiTestHelper<RentalDTO> testHelper;
    @Autowired
    private RestTemplate anonymousUser;
    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private AutosAPI autosClient;
    @Autowired
    private RentersAPI rentersClient;
    @Autowired
    private AutoDTOFactory autoDTOFactory;
    @Autowired
    private RenterDTOFactory renterDTOFactory;
    private @Autowired Environment env;
    @Autowired(required = false)
    private SecurityFilterChain filterChain;

    @BeforeEach
    void check() {
        Assumptions.assumeFalse(getClass().equals(A1_AnonymousAccessNTest.class),"should only run for derived class");
        given(filterChain).as("no security filter chain found").isNotNull();
        given(env.getActiveProfiles()).as("missing required profile")
                .containsAnyOf("anonymous-access","authenticated-access");
    }

    @Test
    void csrf_is_off() {
        then(filterChain.getFilters().stream()
                    .filter(f->f instanceof CsrfFilter)
                    .findFirst().orElse(null))
                .as("CSRF appears to be active")
                .isNull();
    }

    @Nested
    class granted_access_to {
        @Test
        void static_content() {
            //given
            URI url = UriComponentsBuilder.fromUri(serverConfig.getBaseUrl()).path("content/past_transactions.txt").build().toUri();
            RequestEntity<Void> request = RequestEntity.get(url).accept(MediaType.TEXT_PLAIN).build();
            //when
            try {
                ResponseEntity<String> response = anonymousUser.exchange(request, String.class);

                //then
                then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                String body = response.getBody();
                then(body).as("content was empty").isNotNull();
                then(body).as("got login page instead of static content")
                        .doesNotContain("Please sign in");
                then(body).as("did not get expected static content")
                        .contains("Past AutoRentals");
            } catch (HttpClientErrorException.NotFound ex) {
                fail("static content was not found at %s", url);
            }
        }

        @Test
        void autos_safe_head_operation() {
            //when
            HttpStatusCode status;
            try {
                status = autosClient.hasAuto("ANY_ID").getStatusCode();
            } catch (HttpStatusCodeException ex) {
                status = ex.getStatusCode();
            }
            //then
            then(status).as("denied anonymous access").isEqualTo(HttpStatus.NOT_FOUND);
        }

        @Test
        void anyUrl_safe_head_operation() {
            //given
            URI anyUrl = UriComponentsBuilder.fromUri(serverConfig.getBaseUrl()).path("/anyUrl").build().toUri();
            RequestEntity<Void> request = RequestEntity.head(anyUrl).build();
            //when
            HttpStatusCode status;
            try {
                status  = anonymousUser.exchange(request, Void.class).getStatusCode();
            } catch (HttpStatusCodeException ex) {
                status = ex.getStatusCode();
            }
            //then
            then(status).as("denied anonymous access").isEqualTo(HttpStatus.NOT_FOUND);
        }

        @Test
        void autos_safe_get_parent_operation() {
            try {
                //given
                AutoDTO allAutos = AutoDTO.builder().build();
                //when
                ResponseEntity<AutoListDTO> response = autosClient.queryAutos(allAutos, 1, 0);
                //then
                then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                AutoListDTO autos = response.getBody();
            } catch(HttpClientErrorException.Unauthorized | HttpClientErrorException.Forbidden ex) {
                fail("denied anonymous access to root resource: " + ex);
            }
        }

        @Test
        void autos_safe_get_child_operation() {
            assertThatExceptionOfType(HttpClientErrorException.NotFound.class)
                    .as("access not granted to child resource")
                    .isThrownBy(()->autosClient.getAuto("anId"));
        }

        @Test
        void renters_safe_head_operation() {
            //when
            HttpStatusCode status;
            try { //may get a 302 redirect to form login
                status = rentersClient.hasRenter("anId").getStatusCode();
            } catch (HttpStatusCodeException ex) {
                status = ex.getStatusCode();
            }
            //then
            then(status).isEqualTo(HttpStatus.NOT_FOUND);
        }

        @Test
        void rentals_safe_get_parent_operation() {
            try {
                //when
                List<RentalDTO> foundRentals = testHelper.findRentalsBy(SearchParams.builder()
                        .pageNumber(0).pageSize(1)
                        .build());
                //then
                log.info("rentals={}", foundRentals);
            } catch (HttpStatusCodeException ex) {
                fail("denied anonymous access to parent resource: " + ex);
            }
        }
        @Test
        void rentals_safe_get_child_operation() {
            assertThatExceptionOfType(HttpClientErrorException.NotFound.class)
                    .as("denied anonymous access to child resource")
                    .isThrownBy(()->testHelper.getRentalById("anId"));
        }
    }

    @Nested
    class denied_for_access_to {
        @Test
        void autos_nonsafe_post_operation() {
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    () -> autosClient.createAuto(autoDTOFactory.make()),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void autos_nonsafe_put_operation() {
            //given
            AutoDTO auto = autoDTOFactory.make(AutoDTOFactory.withId);
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    () -> autosClient.updateAuto(auto.getId(), auto),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void autos_nonsafe_delete_operation() {
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    ()-> rentersClient.removeAllRenters(),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void renters_unsafe_post_operation() {
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    ()-> rentersClient.createRenter(renterDTOFactory.make()),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void renters_unsafe_put_operation() {
            //given
            RenterDTO renter = renterDTOFactory.make(RenterDTOFactory.withId);
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    ()-> rentersClient.createRenter(renterDTOFactory.make()),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void renters_safe_get_but_secured_get_operation() {
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    ()-> rentersClient.getRenters(null, null),
                    "was not denied access");
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void renters_nonsafe_delete_operation() {
            //when
            HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                    ()-> rentersClient.removeAllRenters());
            //then
            then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
        }

        @Test
        void rentals_nonsafe_delete_operation() {
            //when
            try { //may get a 302 redirect to form login
                HttpStatusCode status = testHelper.removeAllRentals().getStatusCode();
                then(status).as("not denied access").isNotEqualTo(HttpStatus.NO_CONTENT);
                then(status.is2xxSuccessful()).as(()->"not denied access: "+status).isFalse();
            } catch (HttpStatusCodeException ex) {
                then(ex.getStatusCode()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
            }
        }
    }
}
