package info.ejava.assignments.security.autorenters.svc.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import info.ejava.assignments.security.autorenters.svc.AuthorizationHelper;
import info.ejava.examples.common.exceptions.ClientErrorException;
import info.ejava.examples.common.exceptions.ServerErrorException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class SecureRentersServiceWrapper implements RentersService {
    private final RentersService impl;
    private final AuthorizationHelper authzHelper;
    @Override
    public RenterDTO createRenter(RenterDTO newRenter) {
        String username = authzHelper.getUsername().orElseThrow(()->
            new ServerErrorException.InternalErrorException("security has not been enabled")
        );

        Optional<RenterDTO> existingRenter = impl.findRenterByUsername(username);
        if (existingRenter.isPresent()) {
            throw new ClientErrorException.InvalidInputException("renter already exists for %s", username);
        }

        newRenter.setUsername(username);
        return impl.createRenter(newRenter);
    }

    //@PreAuthorize("isAuthenticated()")
    @Override
    public RenterDTO getRenter(String id) {
        RenterDTO renterDTO = impl.getRenter(id);

        String ownerName = renterDTO.getUsername();
        boolean isOwner = authzHelper.isUsername(ownerName);
        authzHelper.assertRules(()->
                        authzHelper.isUsername(ownerName) ||
                        authzHelper.hasAuthority("PROXY") ||
                        authzHelper.assertMgr(),
            (username)->String.format("%s is not this renter and or authorized to get renter[%s]", username, id));
        return renterDTO;
    }

    @Override
    public boolean hasRenter(String id) {
        return impl.hasRenter(id);
    }

    @Override
    public RenterDTO updateRenter(String id, RenterDTO renterDTO) {
        authzHelper.assertUsername(()->impl.getRenter(id).getUsername());
        return impl.updateRenter(id, renterDTO);
    }

    @Override
    public Page<RenterDTO> getRenters(Pageable pageable) {
        return impl.getRenters(pageable);
    }

    @Override
    public Optional<RenterDTO> findRenterByUsername(String username) {
        return impl.findRenterByUsername(username);
    }

    //@PreAuthorize("isAuthenticated()")
    @Override
    public void removeRenter(String id) {
        try {
            String ownername=impl.getRenter(id).getUsername();
            authzHelper.assertRules(()->
                    authzHelper.isUsername(ownername) || authzHelper.assertMgr(),
                    (username)-> String.format("%s is not renter or have MGR role", username));
            impl.removeRenter(id);
        } catch (ClientErrorException.NotFoundException ex) {
            /* already does not exist -- no error */
        }
    }

    //@PreAuthorize("hasRole('ADMIN')")
    @Override
    public void removeAllRenters() {
        impl.removeAllRenters();
    }
}
