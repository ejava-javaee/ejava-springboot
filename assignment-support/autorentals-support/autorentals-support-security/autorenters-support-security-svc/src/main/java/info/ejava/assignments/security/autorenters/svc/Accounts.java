package info.ejava.assignments.security.autorenters.svc;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Accounts {
    private final List<AccountProperties> accounts = new ArrayList<>();
}
