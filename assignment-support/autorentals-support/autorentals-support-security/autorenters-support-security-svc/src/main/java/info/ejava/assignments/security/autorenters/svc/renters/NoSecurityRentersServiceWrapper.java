package info.ejava.assignments.security.autorenters.svc.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class NoSecurityRentersServiceWrapper implements RentersService {
    private final RentersService impl;
    @Override
    public RenterDTO createRenter(RenterDTO newRenter) {
        newRenter.setUsername("anonymous");
        return impl.createRenter(newRenter);
    }

    @Override
    public RenterDTO getRenter(String id) {
        return impl.getRenter(id);
    }

    @Override
    public boolean hasRenter(String id) {
        return impl.hasRenter(id);
    }

    @Override
    public RenterDTO updateRenter(String id, RenterDTO renterDTO) {
        return impl.updateRenter(id, renterDTO);
    }

    @Override
    public Page<RenterDTO> getRenters(Pageable pageable) {
        return impl.getRenters(pageable);
    }

    @Override
    public Optional<RenterDTO> findRenterByUsername(String username) {
        return impl.findRenterByUsername(username);
    }

    @Override
    public void removeRenter(String id) {
        impl.removeRenter(id);
    }

    @Override
    public void removeAllRenters() {
        impl.removeAllRenters();
    }
}
