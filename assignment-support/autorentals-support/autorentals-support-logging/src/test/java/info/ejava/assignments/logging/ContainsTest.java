package info.ejava.assignments.logging;

import org.assertj.core.api.BDDSoftAssertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.BDDAssertions.then;

public class ContainsTest {
    private String text_without_numbers = "09:04:49.654 INFO  [f7f2ce2c-caec-4d47-bb97-df982fcbf947] X.Y - Xxx has started\n" +
            "09:04:49.656 INFO  [f7f2ce2c-caec-4d47-bb97-df982fcbf947] info.ejava_student.assignment1.logging.xxx.svc.XxxsServiceImpl - aId: 2022-01, bId: 528, delta: 20\n" +
            "09:04:49.656 INFO  [f7f2ce2c-caec-4d47-bb97-df982fcbf947] X.Y - aId: 528, bId: 2022-01, delta: 20";
    private String text_with_numbers = "10:29:02.461 INFO  [0e2df699-3e0b-4592-aa33-25e99e4a9aa7] X.Y:run:27 - Xxx has started\n" +
            "10:29:02.461 INFO  [0e2df699-3e0b-4592-aa33-25e99e4a9aa7] info.ejava_student.assignment1.logging.xxx.svc.XxxsServiceImpl:calcDelta:22 - aId: 2022-01, bId: 303, delta: 20\n" +
            "10:29:02.462 INFO  [0e2df699-3e0b-4592-aa33-25e99e4a9aa7] X.Y:run:29 - aId: 303, bId: 2022-01, delta: 20";
    private static final String STRING_PATTERN = "INFO.*(run:[0-9]{1,3}){1}.*has started";
    private static final String TEXT_MATCHES = "(?s).*"+STRING_PATTERN+".*";
    private String text_with_mdc = "10:57:51.534 INFO  [9bd397c5-20ad-4229-a706-06bfe71d9973, 9bd397c5-20ad-4229-a706-06bfe71d9973] X.Y - AutoRentals has started\n";
    private String text_without_mdc = "10:57:51.534 INFO  X.Y - AutoRentals has started\n";
    private static final String MDC_PATTERN = "INFO.+\\[[a-f0-9\\-]{36}.*\\].+has started";
    private static final String MDC_MATCHES = "(?s).*"+MDC_PATTERN+".*";

    @Test
    void not_contain_method_line_number() {
        BDDSoftAssertions softly = new BDDSoftAssertions();
        softly.then(text_without_numbers.matches(TEXT_MATCHES)).isFalse();
        softly.then(text_without_numbers).doesNotContainPattern(STRING_PATTERN);
        softly.assertAll();
    }

    @Test
    void contains_method_line_number() {
        BDDSoftAssertions softly = new BDDSoftAssertions();
        softly.then(text_with_numbers.matches(TEXT_MATCHES)).isTrue();
        softly.then(text_with_numbers).containsPattern(STRING_PATTERN);
        softly.assertAll();
    }

    @Test
    void not_contain_mdc() {
        BDDSoftAssertions softly = new BDDSoftAssertions();
        softly.then(text_without_mdc.matches(MDC_MATCHES)).isFalse();
        softly.then(text_without_mdc).doesNotContainPattern(MDC_PATTERN);
        softly.assertAll();
    }

    @Test
    void contains_mdc() {
        BDDSoftAssertions softly = new BDDSoftAssertions();
        softly.then(text_with_mdc.matches(MDC_MATCHES)).isTrue();
        softly.then(text_with_mdc).containsPattern(MDC_PATTERN);
        softly.assertAll();
    }
}
