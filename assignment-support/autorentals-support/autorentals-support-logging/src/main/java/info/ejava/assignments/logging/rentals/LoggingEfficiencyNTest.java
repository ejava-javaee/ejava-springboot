package info.ejava.assignments.logging.rentals;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.Duration;
import java.time.Instant;

import static org.assertj.core.api.BDDAssertions.then;

@Slf4j
public class LoggingEfficiencyNTest {
    @Nested
    @SpringBootTest
    @DirtiesContext
    @ActiveProfiles("app-debug")
    class when_trace_disabled {
        @Autowired
        CommandLineRunner appCommand;

        @Test
        void should_have_no_delay() {
            Assertions.assertTimeout(Duration.ofSeconds(1), ()->appCommand.run(),
                    "too slow, check for unnecessary logging with app-debug profile");
        }
    }

    @Nested
    @SpringBootTest
    @DirtiesContext
    @ActiveProfiles("repo-only")
    class when_trace_enabled {
        @Autowired
        CommandLineRunner appCommand;

        @Test
        void should_have_delay() throws Exception {
            Instant startTime = Instant.now();
            appCommand.run();
            Duration executionTime = Duration.between(startTime, Instant.now());
            then(executionTime).as("too fast, check toString() an repo-only profile configuration")
                    .isGreaterThan(Duration.ofMillis(1500));
        }
    }
}
