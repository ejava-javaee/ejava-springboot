package info.ejava.assignments.logging.rentals;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan({"info.ejava_student","info.ejava"})
public class LoggingConfiguration {
}
