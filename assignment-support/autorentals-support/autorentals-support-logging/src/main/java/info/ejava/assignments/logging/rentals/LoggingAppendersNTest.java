package info.ejava.assignments.logging.rentals;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.logging.LoggingSystemProperties;
import org.springframework.boot.logging.logback.LogbackLoggingSystem;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.BDDAssertions.*;

public class LoggingAppendersNTest {
    public static final String LOGFILE_NAME = "target/logs/appenders.log";
    private static String readLogfile(int startingPos) throws IOException {
        Path logPath = Path.of(LOGFILE_NAME);
        return Files.isRegularFile(logPath) ? Files.readString(logPath).substring(startingPos) : "";
    }
    private static long getLogfileSize() throws IOException {
        Path logPath = Path.of(LOGFILE_NAME);
        return Files.exists(logPath) ? Files.size(logPath) : 0;
    }



    @Nested
    @SpringBootTest
    @DirtiesContext
    @ActiveProfiles("appenders")
    @ExtendWith(OutputCaptureExtension.class)
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class appenders_profile {
        @Autowired
        private CommandLineRunner appCommand;
        private CapturedOutput console;
        private String logfile;
        private long startingPos;

        @BeforeAll
        void init(CapturedOutput output) throws Exception {
            startingPos = getLogfileSize();
            this.console = output;
            appCommand.run();
            logfile = readLogfile((int)startingPos);
        }
        @AfterAll
        void cleanup() {
            //clear any previous setting of logging.pattern.file, by LoggingSystemProperties.apply()
            // when processing 2nd SprintBootTest; force it to be re-resolved from property file
            System.clearProperty("FILE_LOG_PATTERN");
        }

        @Test
        void logfile_exists() {
            then(new File(LOGFILE_NAME)).as(()->"missing appender logfile: " + LOGFILE_NAME).exists();
        }
        @Test
        void logfile_has_data() throws IOException {
            then(getLogfileSize()).as("logfile is empty").isNotZero();
        }
        @Test
        void has_no_trace_console() {
            then(console).as("unexpected logs in CONSOLE").doesNotContain("TRACE");
        }
        @Test
        void has_no_debug_console() {
            then(console).as("unexpected logs in CONSOLE").doesNotContain("DEBUG");
        }

        @Test
        void has_no_trace_file() {
            then(logfile).as("unexpected logs in FILE").doesNotContain("TRACE");
        }
        @Test
        void has_no_debug_file() {
            then(logfile).as("unexpected logs in FILE").doesNotContain("DEBUG");
        }

        @Test
        void has_no_repo_logs_in_console() {
            then(console).as("unexpected RepoImpl logs in CONSOLE").doesNotContain("RepositoryImpl");
        }
        @Test
        void has_no_repo_logs_in_logfile() {
            then(logfile).as("unexpected RepoImpl logs in FILE").doesNotContain("RepositoryImpl");
        }
        @Test
        void has_xy_logs_in_console() {
            then(console).as("CONSOLE missing X.Y logs").containsPattern("INFO.+X\\.Y");
        }
        @Test
        void has_xy_logs_in_logfile() {
            then(logfile).as("FILE missing X.Y logs").containsPattern("INFO.+Y");
        }
        @Test
        void has_svc_logs_in_console() {
            then(console).as("CONSOLE missing ServiceImpl logs").containsPattern("INFO.+ServiceImpl");
        }
        @Test
        void has_svc_logs_in_logfile() {
            then(logfile).as("FILE missing ServiceImpl logs").containsPattern("INFO.+ServiceImpl");
        }
        @Test
        void has_no_line_numbers_in_logfile() {
            then(logfile).as("FILE should contain no method and line numbers")
                    .doesNotContainPattern("INFO.+(run:[0-9]{1,3}).+has started");
        }
    }

    @Nested
    @SpringBootTest
    @DirtiesContext
    @ActiveProfiles({"appenders","trace"})
    @ExtendWith(OutputCaptureExtension.class)
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class appenders_trace_profile {
        @Autowired
        private CommandLineRunner appCommand;
        private CapturedOutput console;
        private String logfile;
        private long startingPos;

        @BeforeAll
        void init(CapturedOutput output) throws Exception {
            startingPos = getLogfileSize();
            this.console = output;
            appCommand.run();
            logfile = readLogfile((int)startingPos);
        }
        @AfterAll
        void cleanup() {
            //clear any previous setting of logging.pattern.file, by LoggingSystemProperties.apply()
            // when processing 2nd SprintBootTest; force it to be re-resolved from property file
            System.clearProperty("FILE_LOG_PATTERN");
        }

        @Test
        void logfile_exists() {
            then(new File(LOGFILE_NAME)).as(()->"missing appender logfile: " + LOGFILE_NAME).exists();
        }
        @Test
        void logfile_has_data() throws IOException {
            then(getLogfileSize()).as("logfile is empty").isNotZero();
        }
        @Test
        void has_no_repo_logs_in_console() {
            then(console).as("unexpected RepoImpl logs in CONSOLE").doesNotContain("RepositoryImpl");
        }
        @Test
        void has_repo_logs_in_logfile() {
            then(logfile).as("FILE missing RepoImpl logs").containsPattern("TRACE.+RepositoryImpl");
        }
        @Test
        void has_xy_logs_in_console() {
            then(console).as("CONSOLE missing X.Y logs").containsPattern("INFO.+X\\.Y");
        }
        @Test
        void has_xy_logs_in_logfile() {
            then(logfile).as("FILE missing X.Y logs").containsPattern("INFO.+Y");
        }

        @Test
        void mdc_not_in_console() {
            then(console).as("CONSOLE contains MDC")
                    .doesNotContainPattern("INFO.+\\[[a-f0-9\\-]{36}.*\\].+has started");
        }
        @Test
        void mdc_in_logfile() {
            then(logfile).as("FILE missing MDC")
                    .containsPattern("INFO.+\\[[a-f0-9\\-]{36}.*\\].+has started");
        }
        @Test
        void line_numbers_in_logfile() {
            then(logfile).as("FILE should have method and line numbers")
                    .containsPattern("INFO.+(run:[0-9]{1,3}).+has started");
        }
    }
}
