package info.ejava.assignments.db.autorenters.svcmongo.renters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data

@Document(collection="renters")
public class RenterBO {
    @Id //org.springframework.data.annotation.Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate dob;
    private String email;
    private String username;
}