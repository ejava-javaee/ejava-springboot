package info.ejava.assignments.db.autorenters.svcmongo.renters;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RentersMongoRepository extends MongoRepository<RenterBO, String> {
    Optional<RenterBO> findRenterByUsername(String username);
}
