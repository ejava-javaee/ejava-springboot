package info.ejava.assignments.db.autorenters.svcmongo.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import info.ejava.examples.common.exceptions.ClientErrorException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class RentersRepoServiceImpl implements RentersService {
    private final RentersMongoRepository repo;
    private final RentersMapper mapper;

    @Override
    public RenterDTO createRenter(RenterDTO newRenter) {
        if (null==newRenter) {
            throw new ClientErrorException.InvalidInputException("renter is required");
        }
        if (null!=newRenter.getId()) {
            throw new ClientErrorException.InvalidInputException("renter.id must be null");
        }

        RenterBO savedRenterBO = repo.save(mapper.map(newRenter));
        RenterDTO savedRenter = mapper.map(savedRenterBO);
        log.debug("added renter: {}", savedRenter);
        return savedRenter;
    }

    @Override
    public RenterDTO getRenter(String id) {
        return repo.findById(id)
                .map(dto->mapper.map(dto))
                .orElseThrow(()->{
                    log.debug("getRenter({}) not found", id);
                    throw new ClientErrorException.NotFoundException("Renter[%s] not found", id);
                });
    }

    @Override
    public boolean hasRenter(String id) {
        return repo.existsById(id);
    }

    @Override
    public RenterDTO updateRenter(String id, RenterDTO updateRenter) {
        if (null==updateRenter) {
            throw new ClientErrorException.InvalidInputException("renter is required");
        }
        if (null!=id) {
            updateRenter.setId(id);
        }
        log.debug("updating renter id={}, {}", id, updateRenter);
        RenterBO savedRenterBO = repo.save(mapper.map(updateRenter));
        return mapper.map(savedRenterBO);
    }

    @Override
    public Page<RenterDTO> getRenters(Pageable pageable) {
        Page<RenterBO> renters = repo.findAll(pageable);
        if (pageable.isPaged()) {
            log.debug("pageSize {}/pageNumber {}, returns {}", pageable.getPageSize(), pageable.getPageNumber(), renters.getNumberOfElements());
        } else {
            log.debug("nonPaged returns {}", renters.getNumberOfElements());
        }
        return renters.map(bo->mapper.map(bo));
    }

    @Override
    public Optional<RenterDTO> findRenterByUsername(String username) {
        return repo.findRenterByUsername(username)
                .map(bo->mapper.map(bo));
    }

    @Override
    public void removeRenter(String id) {
        repo.deleteById(id);
    }

    @Override
    public void removeAllRenters() {
        repo.deleteAll();
    }

}
