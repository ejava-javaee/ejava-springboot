package info.ejava.assignments.db.autorenters.svcmongo.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;

public class RentersMapper {
    public RenterBO map(RenterDTO dto) {
        RenterBO bo = null;
        if (null!=dto) {
            bo = RenterBO.builder()
                    .id(dto.getId())
                    .firstName(dto.getFirstName())
                    .lastName(dto.getLastName())
                    .email(dto.getEmail())
                    .dob(dto.getDob())
                    .username(dto.getUsername())
                    .build();
        }
        return bo;
    }

    public RenterDTO map(RenterBO bo) {
        RenterDTO dto = null;
        if (null!=bo) {
            dto = RenterDTO.builder()
                    .id(bo.getId())
                    .firstName(bo.getFirstName())
                    .lastName(bo.getLastName())
                    .email(bo.getEmail())
                    .dob(bo.getDob())
                    .username(bo.getUsername())
                    .build();
        }
        return dto;
    }


}
