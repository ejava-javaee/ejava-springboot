package info.ejava.assignments.db.autorenters.svcmongo.renters;

import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration(proxyBeanMethods = false)
@EnableMongoRepositories(basePackageClasses = {RentersMongoRepository.class})
@ConditionalOnClass(MongoRepository.class)
public class RentersMongoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    RentersMapper renterMapper() {
        return new RentersMapper();
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public RentersService rentersRepoService(RentersMongoRepository rentersMongoRepository, RentersMapper mapper) {
        return new RentersRepoServiceImpl(rentersMongoRepository, mapper);
    }
}