package info.ejava.assignments.db.autorenters.svcmongo.renters;

import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.api.autorenters.dto.renters.RenterListDTO;
import info.ejava.assignments.db.autorenters.svcmongo.MongoPageableAutoRenterTestConfiguration;
import info.ejava.assignments.db.autorenters.svcmongo.testapp.DbSecurityTestConfiguration;
import info.ejava.assignments.security.autorenters.svc.ProvidedAuthorizationTestHelperConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDate;

import static org.assertj.core.api.BDDAssertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * This test focuses on primary calls that hit basic CRUD
 * in the MongoRepository.
 */
@SpringBootTest(classes={
        ProvidedAuthorizationTestHelperConfiguration.class,
        DbSecurityTestConfiguration.class,
        MongoPageableAutoRenterTestConfiguration.class
    },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootConfiguration
@EnableAutoConfiguration
@ActiveProfiles({"authorities","test"})
public class RentersRepoNTest {
    @Autowired
    private RenterDTOFactory renterFactory;
    @Autowired @Qualifier("authnRentersClient")
    private RentersAPI authnClient;

    @BeforeEach
    @AfterEach
    void cleanup(@Autowired RentersAPI adminRentersClient) {
        adminRentersClient.removeAllRenters();
    }

    @Test
    void get_renter() {
        //verify
        assertThrows(HttpClientErrorException.NotFound.class,
                () -> authnClient.hasRenter("aRenterId"));
    }

    @Test
    void check_renter_exists() {
        //when
        HttpClientErrorException ex = catchThrowableOfType(
                () -> authnClient.hasRenter("aRenterId"),
                HttpClientErrorException.class);
        //then
        then(ex).as("no exception thrown").isNotNull();
        then(ex.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void get_renters() {
        //when
        ResponseEntity<RenterListDTO> response = authnClient.getRenters(0, 1);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void add_renter() {
        //given
        RenterDTO renter = renterFactory.make();
        //when
        ResponseEntity<RenterDTO> response = authnClient.createRenter(renter);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        then(response.getBody().getId()).isNotNull();
    }

    @Test
    void update_renter() {
        //given
        RenterDTO renter = authnClient.createRenter(renterFactory.make()).getBody();
        LocalDate modifiedDob = renter.getDob().plusYears(1);
        RenterDTO modifiedRenter = renter.withDob(modifiedDob);
        //when
        ResponseEntity<RenterDTO> response = authnClient.updateRenter(renter.getId(), modifiedRenter);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody().getDob()).isEqualTo(modifiedDob);
    }
    @Test
    void remove_their_renter() {
        //given
        RenterDTO renter = authnClient.createRenter(renterFactory.make()).getBody();
        //when
        ResponseEntity<Void> response = authnClient.removeRenter(renter.getId());
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}
