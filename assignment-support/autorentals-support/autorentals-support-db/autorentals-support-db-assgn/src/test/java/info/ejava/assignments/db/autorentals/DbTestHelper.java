package info.ejava.assignments.db.autorentals;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface DbTestHelper<D extends RentalDTO, B extends RentalBO> extends ApiTestHelper<D> {
    /**
     * @return RDBMS BO table name
     */
    String getRentalsTableName();
    /**
     * @return JPA EntityName for the BO
     */
    String getRentalsEntityName();

    /**
     * @return Mongo collection name for BO instances
     */
    String getRentalsCollectionName();

    /**
     * Builds a fully-populated, completed AutoRental DTO based on the information that
     * is provided by the supplied DTOs. This mapping is a server-side representation
     * and should include the username field.
     * @param autoDTO
     * @param renterDTO
     * @return AutoRentalDTO in a server-side completed state
     */
    D makeFullRental(AutoDTO autoDTO, RenterDTO renterDTO, TimePeriod timePeriod, String username);

    /**
     * Builds a AutoRental DTO with only the autoId. All other fields must be null.
     * @param autoDTO
     * @return AutoRental DTO populated with only the autoId supplied in the auto
     */
    D makeAutoIdProbe(AutoDTO autoDTO);

    /**
     * @param dto
     * @return username from the DTO
     */
    String getUsername(D dto);

    //basic getters against the BO
    String getRentalId(B bo);
    String getAutoId(B bo);
    String getRenterId(B bo);
    LocalDate getStartDate(B bo);
    LocalDate getEndDate(B bo);

    BigDecimal getAmount(B bo);
    String getMakeModel(B bo);
    Integer getRenterAge(B bo);
    String getRenterName(B bo);
    String getStreet(B bo);
    String getCity(B bo);
    String getState(B bo);
    String getZip(B bo);

    String getUsername(B bo);
}
