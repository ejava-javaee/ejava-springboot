package info.ejava.assignments.db.autorentals;

import info.ejava.assignments.api.autorenters.dto.StreetAddressDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalsMapper;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * This class is used by both the JPA and Mongo tests to verify the mapper is fully functional
 * before using.
 */
@RequiredArgsConstructor
public class MapperTestHelper {
    private final RentalsMapper mapper;
    private final AutoDTOFactory autoFactory;
    private final RenterDTOFactory renterFactory;
    private final DbTestHelper testHelper;

    public void mapper_can_map_dto_to_bo() {
        //given
        RentalDTO dto = given_fully_populated_rental_dto();
        //when
        RentalBO bo = mapper.map(dto);
        //then
        then(bo).as("DTO to BO mapper returned null").isNotNull();
        then(getAllProperties(bo))
                .allSatisfy((k, v) -> then(v)
                        .as(()->String.format("DTO mapping to rentalBO.%s", k))
                        .isNotNull());
        then(testHelper.getRentalId(bo)).as("BO.id is null, check testHelper").isNotNull();
        then(testHelper.getAutoId(bo)).as("BO.autoId is null, check testHelper").isNotNull();
        then(testHelper.getRenterId(bo)).as("BO.renterId is null, check testHelper").isNotNull();

        then(testHelper.getRentalId(bo)).as("mapping to BO.rentalId").isEqualTo(testHelper.getRentalId(dto));
        then(testHelper.getAutoId(bo)).as("mapping to BO.autoId").isEqualTo(testHelper.getAutoId(dto));
        then(testHelper.getStartDate(bo)).as("mapping to BO.startDate").isEqualTo(testHelper.getStartDate(dto));
        then(testHelper.getEndDate(bo)).as("mapping to BO.endDate").isEqualTo(testHelper.getEndDate(dto));
        then(testHelper.getRenterAge(bo)).as("mapping to BO.renterAge").isEqualTo(testHelper.getRenterAge(dto));
        then(testHelper.getAmount(bo)).as("mapping to BO.amount").isEqualTo(testHelper.getAmount(dto));
        then(testHelper.getMakeModel(bo)).as("mapping to BO.makeModel").isEqualTo(testHelper.getAutoMakeModel(dto));
        then(testHelper.getRenterId(bo)).as("mapping to BO.renterId").isEqualTo(testHelper.getRenterId(dto));
        then(testHelper.getRenterName(bo)).as("mapping to BO.renterName").isEqualTo(testHelper.getRenterName(dto));
        then(testHelper.getUsername(bo)).as("mapping to BO.username").isEqualTo(testHelper.getUsername(dto));
        StreetAddressDTO address = testHelper.getStreetAddress(dto);
        then(address).as("DTO address").isNotNull();
        then(testHelper.getStreet(bo)).as("mapping to BO.street").isEqualTo(address.getStreet());
        then(testHelper.getCity(bo)).as("mapping to BO.city").isEqualTo(address.getCity());
        then(testHelper.getState(bo)).as("mapping to BO.state").isEqualTo(address.getState());
        then(testHelper.getZip(bo)).as("mapping to BO.zip").isEqualTo(address.getZip());
    }

    public void mapper_can_map_bo_to_dto() {
        //given
        RentalBO bo = given_fully_populated_rental_bo();
        //when
        RentalDTO dto = mapper.map(bo);
        //then
        then(bo).as("BO to DTO mapper returned null").isNotNull();
        then(getAllProperties(bo))
                .allSatisfy((k, v) -> then(v)
                        .as(()->String.format("BO mapping to rentalDTO.%s", k))
                        .isNotNull());
        then(getAllProperties(dto)).allSatisfy((k, v) -> then(v).as("rentalDTO: "+k).isNotNull());
        then(testHelper.getRentalId(dto)).isEqualTo(testHelper.getRentalId(bo));
        then(testHelper.getAutoId(dto)).as("mapping to DTO.autoId").isEqualTo(testHelper.getAutoId(bo));
        then(testHelper.getStartDate(dto)).as("mapping to DTO.startDate").isEqualTo(testHelper.getStartDate(bo));
        then(testHelper.getEndDate(dto)).as("mapping to DTO.endDate").isEqualTo(testHelper.getEndDate(bo));
        then(testHelper.getRenterAge(dto)).as("mapping to DTO.renterAge").isEqualTo(testHelper.getRenterAge(bo));
        then(testHelper.getAmount(dto)).as("mapping to DTO.amount").isEqualTo(testHelper.getAmount(bo));
        then(testHelper.getRenterId(dto)).as("mapping to DTO.renterId").isEqualTo(testHelper.getRenterId(bo));
        then(testHelper.getRenterName(dto)).as("mapping to DTO.renterName").isEqualTo(testHelper.getRenterName(bo));
        then(testHelper.getUsername(dto)).as("mapping to DTO.username").isEqualTo(testHelper.getUsername(bo));
        StreetAddressDTO address = testHelper.getStreetAddress(dto);
        then(address).isNotNull();
        then(address.getStreet()).as("mapping to DTO.street").isEqualTo(testHelper.getStreet(bo));
        then(address.getCity()).as("mapping to DTO.city").isEqualTo(testHelper.getCity(bo));
        then(address.getState()).as("mapping to DTO.state").isEqualTo(testHelper.getState(bo));
        then(address.getZip()).as("mapping to DTO.zip").isEqualTo(testHelper.getZip(bo));
    }


    /**
     * Return a map of values keyed by propertyName.
     */
    private Map<String, Object> getAllProperties(Object object) {
        Map<String, Object> properties = new HashMap<>();
        for (Method m: object.getClass().getDeclaredMethods()) {
            if (m.getName().startsWith("get") && m.getParameterCount()==0) {
                try {
                    Object value = m.invoke(object);
                    String name = m.getName().substring(3);
                    name = name.substring(0,1).toLowerCase() + name.substring(1);
                    properties.put(name, value);
                } catch (IllegalAccessException | InvocationTargetException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        return properties;
    }

    private RentalDTO given_fully_populated_rental_dto() {
        //given
        RentalDTO dto = testHelper.makePopulatedFake();
        Map<String, Object> dtoProperties = getAllProperties(dto);
        then(dtoProperties)
                .allSatisfy((k, v) -> then(v)
                        .as(() -> String.format("non-null rentalDTO.%s needed for server-side DTO/BO testing", k))
                        .isNotNull());
        StreetAddressDTO address = testHelper.getStreetAddress(dto);
        then(address).isNotNull();
        then(getAllProperties(address))
                .allSatisfy((k, v) -> then(v)
                        .as(() -> String.format("non-null addressDTO.%s is needed for server-side DTO/BO testing", k))
                        .isNotNull());
        return dto;
    }

    private RentalBO given_fully_populated_rental_bo() {
        //given
        RentalDTO dto = given_fully_populated_rental_dto();
        //when
        RentalBO bo = mapper.map(dto);
        //then
        Map<String, Object> boProperties = getAllProperties(bo);
        then(boProperties)
                .allSatisfy((k, v) -> then(v)
                        .as(() -> String.format("non-null rentalBO.%s needed for server-side BO/DTO testing", k))
                        .isNotNull());
        return bo;
    }
}
