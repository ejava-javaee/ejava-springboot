package info.ejava.assignments.db.autorentals;

import com.mongodb.client.MongoClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssumptions.given;

//@SpringBootTest(classes={DbAssignmentTestConfiguration.class,
//        MongoAssignmentDBConfiguration.class,
//        DbClientTestConfiguration.class
//})
//@ActiveProfiles(profiles={"assignment-tests","test"}, resolver = TestProfileResolver.class)
////@ActiveProfiles(profiles={"assignment-tests","test", "mongodb"})
public class Mongo5a_ClientTest {
    @Autowired(required = false)
    private MongoOperations mongoOperations;
    @Autowired(required = false)
    private MongoClient mongoClient;

    @BeforeEach
    void check() {
        then(mongoOperations).as("mongoOps not injected, check MongoDB configuration\"").isNotNull();
        then(mongoClient).as("mongoClient not injected, check MongoDB configuration").isNotNull();
    }

    @Test
    void has_mongo_template_injected() {
        long count = mongoOperations.count(new Query(), "some_collection");
        then(count).isEqualTo(0);
        String shortDescription = mongoClient.getClusterDescription().getShortDescription();
        then(shortDescription).contains("state=CONNECTED");
    }
}
