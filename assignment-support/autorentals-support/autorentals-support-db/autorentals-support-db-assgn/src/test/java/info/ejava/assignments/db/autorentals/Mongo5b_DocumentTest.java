package info.ejava.assignments.db.autorentals;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.db.autorenters.rentals.MongoAssignmentService;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalsMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.SecureRandom;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;

//@SpringBootTest(classes={DbAssignmentTestConfiguration.class,
//        MongoAssignmentDBConfiguration.class,
//        DbClientTestConfiguration.class
//})
//@ActiveProfiles(profiles={"assignment-tests","test"}, resolver = TestProfileResolver.class)
////@ActiveProfiles(profiles={"assignment-tests","test", "mongodb"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Mongo5b_DocumentTest {
    @Autowired
    private DbTestHelper<RentalDTO, RentalBO> testHelper;
    @Autowired
    private RentalsMapper mapper;
    @Autowired
    private AutoDTOFactory autoFactory;
    @Autowired
    private RenterDTOFactory renterFactory;
    @Autowired
    private MongoAssignmentService assignmentService;
    private MapperTestHelper mapperTestHelper;

    @BeforeEach
    void init() {
        mapperTestHelper = new MapperTestHelper(mapper, autoFactory, renterFactory, testHelper);
    }

    @Test
    @Order(0)
    void mapper_can_map_dto_to_bo() {
        mapperTestHelper.mapper_can_map_dto_to_bo();
    }

    @Test
    @Order(1)
    void mapper_can_map_bo_to_dto() {
        mapperTestHelper.mapper_can_map_bo_to_dto();
    }

    @Test
    @Order(100)
    void map_persist_query_byDate() throws SQLException {
        //given
        AutoDTO auto = autoFactory.make(AutoDTOFactory.withId).withUsername(autoFactory.username());
        RenterDTO renter = renterFactory.make(RenterDTOFactory.withId);
        SecureRandom random = new SecureRandom();
        TimePeriod timePeriod = new TimePeriod(LocalDate.now().plusDays(random.nextInt(100)),
                random.nextInt(6)+1);
        String username = "mannypep";
        for (int i=0; i<3; i++) {
            RentalDTO fullRentalDTO = testHelper.makeFullRental(auto, renter, timePeriod, username);
            then(fullRentalDTO).as("check testHelper.makeCompletedRental").isNotNull();

            //when
            assignmentService.mapAndPersist(fullRentalDTO);
            List<RentalDTO> results = assignmentService.queryByRentalDateRange(timePeriod.getStartDate(), timePeriod.getEndDate());

            //then
            Optional<RentalDTO> result = results.stream()
                    .filter(r -> auto.getId().equals(testHelper.getAutoId(r)))
                    .filter(r -> renter.getId().equals(testHelper.getRenterId(r)))
                    .findFirst();
            then(result).as("intended entity not returned").isPresent();
            then(results.size()).as("unexpected number of results for loop " + i).isEqualTo(1);
            timePeriod = timePeriod.next();
        }
    }

}
