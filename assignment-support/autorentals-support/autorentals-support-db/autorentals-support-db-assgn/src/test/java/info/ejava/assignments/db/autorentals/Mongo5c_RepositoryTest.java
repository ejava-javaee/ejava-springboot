package info.ejava.assignments.db.autorentals;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.db.autorenters.rentals.MongoAssignmentService;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalsMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.then;

//@SpringBootTest(classes={DbAssignmentTestConfiguration.class,
//        MongoAssignmentDBConfiguration.class,
//        DbClientTestConfiguration.class
//})
//@ActiveProfiles(profiles={"assignment-tests","test"}, resolver = TestProfileResolver.class)
////@ActiveProfiles(profiles={"assignment-tests","test", "mongodb"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class Mongo5c_RepositoryTest {
    @Autowired
    private DbTestHelper<RentalDTO, RentalBO> testHelper;
    @Value("${spring.data.mongodb.uri:(not supplied)}")
    private String mongoUrl;
    @Autowired
    private MongoOperations mongoOps;
    @Autowired
    private RentalsMapper mapper;
    @Autowired
    private AutoDTOFactory autoFactory;
    @Autowired
    private RenterDTOFactory renterFactory;
    @Autowired
    private MongoAssignmentService studentAssignment;
    private List<RentalDTO> rentals = new ArrayList<>();
    private BigDecimal commonDailyRate = BigDecimal.valueOf(1_000);
    //this auto has been rented many times
    private String commonAutoId;
    //this auto has been rented for the same amount as many others
    private AutoDTO commonAutoDTO;
    private LocalDate minDate;
    private LocalDate maxDate;
    private BigDecimal commonAmount;


    @BeforeEach
    void init() {
        log.info("mongoURL={}", mongoUrl);
    }


    @Test
    void can_find_by_autoId_by_derived_query() throws SQLException {
        //given
        List<RentalDTO> matches = getOrderedMatches(r -> commonAutoId.equals(testHelper.getAutoId(r)));
        Pageable pageable = PageRequest.of(3, 2, Sort.by("id").ascending());
        log.debug("min={}, max={}, pageable={}, matches=\n{}", minDate, maxDate, pageable,
                StringUtils.join(matches,System.lineSeparator()));
        Set<String> expectedRegistrationIds = getRentalIds(matches, pageable);

        //when
        Page<RentalBO> page = studentAssignment.findByAutoIdByDerivedQuery(commonAutoId, pageable);

        //then
        then(page).as("SVC findByAutoIdByDerivedQuery() not implemented, returning null").isNotNull();
        then(page).hasSizeLessThanOrEqualTo(pageable.getPageSize());
        Set<String> foundRenterIds = page.stream().map(bo->testHelper.getRentalId(bo).toString()).collect(Collectors.toSet());
        then(foundRenterIds).isEqualTo(expectedRegistrationIds);
    }

    @Test
    void can_find_with_amount_by_example() {
        //given
        RentalDTO probeDTO = testHelper.makeAutoIdProbe(commonAutoDTO);
        then(probeDTO).as("testHelper returned null probe DTO").isNotNull();
        RentalBO probe = mapper.map(probeDTO);
        List<RentalDTO> matches = getOrderedMatches(
                r -> commonAutoDTO.getId().equals(testHelper.getAutoId(r)) &&
                        commonAmount.equals(testHelper.getAmount(r)));
        Pageable pageable = PageRequest.of(1, 2, Sort.by("id").ascending());
        log.debug("min={}, max={}, pageable={}, matches=\n{}", minDate, maxDate, pageable,
                StringUtils.join(matches,System.lineSeparator()));
        Set<String> expectedRentalIds = getRentalIds(matches, pageable);

        //when
        Page<RentalBO> page = studentAssignment.findByExample(probe, pageable);

        //then
        then(page).as("SVC findByExample() not implemented, returning null").isNotNull();
        then(page).hasSizeLessThanOrEqualTo(pageable.getPageSize());
        Set<String> foundRenterIds = page.stream().map(bo->testHelper.getRentalId(bo).toString()).collect(Collectors.toSet());
        then(foundRenterIds).isEqualTo(expectedRentalIds);
    }


    @Test
    void can_find_rentals_within_date_range_by_annotated_query() {
        //given
        List<RentalDTO> matches = getOrderedMatches(
                r->!testHelper.getStartDate(r).isBefore(minDate) && !testHelper.getEndDate(r).isAfter(maxDate));
        Pageable pageable = PageRequest.of(1, 2, Sort.by("id").ascending());
        log.debug("min={}, max={}, pageable={}, matches=\n{}", minDate, maxDate, pageable,
                StringUtils.join(matches,System.lineSeparator()));
        Set<String> expectedRegistrationIds = getRentalIds(matches, pageable);

        //when
        Slice<RentalBO> slice = studentAssignment.findByDateRangeByAnnotatedQuery(minDate, maxDate, pageable);

        //then
        then(slice).as("SVC findByDateRangeByAnnotatedQuery() not implemented, returning null").isNotNull();
        then(slice).hasSizeLessThanOrEqualTo(pageable.getPageSize());
        Set<String> foundRenterIds = slice.stream().map(bo->testHelper.getRentalId(bo).toString()).collect(Collectors.toSet());
        then(foundRenterIds).isEqualTo(expectedRegistrationIds);
    }


    @AfterAll
    void cleanup() {
        log.info("cleaning up collection: {}", testHelper.getRentalsCollectionName());
        Class<?> entityClass = mapper.map(testHelper.makePopulatedFake()).getClass();
        mongoOps.remove(new Query(), entityClass);
        assertThat(mongoOps.count(new Query(), entityClass)).isZero();
    }

    @BeforeAll
    void populate() {
        cleanup();
        //create multiple rentals for the same auto
        AutoDTO autoDTO = autoFactory.make(AutoDTOFactory.withId).withUsername(autoFactory.username());
        commonAutoId = autoDTO.getId();
        TimePeriod timePeriod = new TimePeriod(LocalDate.now(),7);
        for (int i=0; i<10; i++) {
            RenterDTO renterDTO = renterFactory.make(RenterDTOFactory.withId);
            RentalDTO dto = testHelper.makeFullRental(autoDTO, renterDTO, timePeriod, renterFactory.username());
            timePeriod = timePeriod.next();
            testHelper.setRentalId(dto, null);
            RentalBO bo = mapper.map(dto);
            mongoOps.save(bo);
            rentals.add(mapper.map(bo));
        }

        //create multiple rentals with the same amount
        for (int i=0; i<15; i++) {
            if (i%5==0) {
                autoDTO = autoFactory.make(AutoDTOFactory.withId)
                        .withPassengers(i%2==0? 6:8)
                        .withUsername(autoFactory.username())
                        .withDailyRate(commonDailyRate);
                commonAutoDTO = autoDTO;
            }
            RenterDTO renterDTO = renterFactory.make(RenterDTOFactory.withId);
            RentalDTO dto = testHelper.makeFullRental(autoDTO, renterDTO, timePeriod, renterFactory.username());
            if (i%5==0) {
                commonAmount = testHelper.getAmount(dto);
            }
            timePeriod = timePeriod.next();
            testHelper.setRentalId(dto, null);
            RentalBO bo = mapper.map(dto);
            mongoOps.save(bo);
            rentals.add(mapper.map(bo));
        }

        List<TimePeriod> timePeriods = rentals.stream()
                .map(s -> testHelper.getTimePeriod(s))
                .sorted()
                .toList();
        int mid = timePeriods.size()/2;
        minDate = timePeriods.get(mid-3).getStartDate();
        maxDate = timePeriods.get(mid+3).getEndDate();
        log.info("count={}, minAge={}, maxAge={}", rentals.size(), minDate, maxDate);
    }

    private List<RentalDTO> getOrderedMatches(Predicate<RentalDTO> predicate){
        return rentals.stream()
                .filter(r -> predicate.test(r))
                .sorted((lhs,rhs)->testHelper.getRentalId(lhs).compareTo(testHelper.getRentalId(rhs)))
                .toList();
    }

    private Set<String> getRentalIds(List<RentalDTO> matches, Pageable pageable) {
        log.debug("pageable={}", pageable);
        return matches
                .subList(pageable.getPageNumber()*pageable.getPageSize(),
                        (pageable.getPageNumber()+1)*pageable.getPageSize()).stream()
                .map(dto -> testHelper.getRentalId(dto))
                .collect(Collectors.toSet());
    }
}