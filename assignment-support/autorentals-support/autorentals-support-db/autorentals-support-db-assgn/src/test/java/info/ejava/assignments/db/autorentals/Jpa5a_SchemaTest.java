package info.ejava.assignments.db.autorentals;

import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssumptions.given;

//@SpringBootTest(classes={DbAssignmentTestConfiguration.class,
//        JpaAssignmentDBConfiguration.class,
//        DbClientTestConfiguration.class})
//@ActiveProfiles(profiles={"assignment-tests","test"}, resolver = TestProfileResolver.class)
////@ActiveProfiles(profiles={"assignment-tests","test", "postgres"})
@Slf4j
public class Jpa5a_SchemaTest {
    @Autowired
    private DbTestHelper<RentalDTO, RentalBO> testHelper;
    @Autowired(required = false)
    DataSource dataSource;
    String tableName;

    @BeforeEach
    void check() {
        given(testHelper).as("testHelper not supplied").isNotNull();
        given(dataSource).as("no dataSource is configured").isNotNull();
        tableName = testHelper.getRentalsTableName();
        given(tableName).as("tableName not supplied").isNotNull();
    }

    @Test
    void has_DataSource() throws SQLException {
        //given
        String url = dataSource.getConnection().getMetaData().getURL();
        log.info("dbUrl={}", url);
        //then
        then(url).startsWith("jdbc");
    }

    @Test
    void has_rentals_table() throws SQLException {
        //given
        String countQuery = String.format("select count(*) from %s", tableName);
        log.debug("test query={}", countQuery);
        //when
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(countQuery);
            ResultSet rs = statement.executeQuery()) {
            rs.next();
            log.info("table[{}] exists", tableName);
        }
    }

    @Test
    void has_rentals_sample_row() throws SQLException {
        //given
        String countQuery = String.format("select count(*) from %s", tableName);
        log.debug("test query={}", countQuery);
        //when
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(countQuery);
             ResultSet rs = statement.executeQuery()) {
            rs.next();
            int rows = rs.getInt(1);
            log.info("table[{}] exists with {} rows", tableName, rows);
            then(rows).isPositive();
        }
    }
}
