package info.ejava.assignments.db.autorenters.rentals;

import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.db.autorenters.svc.rentals.RentalBO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import java.time.LocalDate;
import java.util.List;

public interface JpaAssignmentService<D extends RentalDTO, B extends RentalBO> {
    D mapAndPersist(D rentalDTO);
    List<D> queryByRentalDateRange(LocalDate startInclusive, LocalDate endInclusive);

    Page<B> findByAutoIdByDerivedQuery(String autoId, Pageable pageable);
    Page<B> findByExample(B probe, Pageable pageable);
    Slice<B> findByDateRangeByAnnotatedQuery(LocalDate startDate, LocalDate endDate, Pageable pageable);
}
