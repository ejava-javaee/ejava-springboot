package info.ejava.assignments.db.autorenters.client.renters;

import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface RentersPageableAPI extends RentersAPI {
    public static final String BUYERS_PAGED_PATH = "/api/renters/paged";

    ResponseEntity<RenterPageDTO> getRentersPaged(Pageable pageable);
}
