package info.ejava.assignments.db.autorenters.client.autos;

import info.ejava.assignments.api.autorenters.client.autos.AutosAPI;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoSearchParams;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface AutosPageableAPI extends AutosAPI {
    String AUTOS_PAGED_QUERY_PATH = "/api/autos/query/paged";
    String AUTOS_PAGED_PATH = "/api/autos/paged";

    ResponseEntity<AutoPageDTO> queryAutosPaged(AutoDTO probe, Pageable pageable);
    ResponseEntity<AutoPageDTO> searchAutosPaged(AutoSearchParams searchParams, Pageable pageable);
}
