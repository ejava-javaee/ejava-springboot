package info.ejava.assignments.db.autorenters.client.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.examples.common.dto.paging.PageDTO;
import org.springframework.data.domain.Page;

/**
 * This DTO fully represents a page of results. It is included here
 * to be consistent with topics later in semester.
 */
public class RenterPageDTO extends PageDTO<RenterDTO> {
    protected RenterPageDTO(){}
    public RenterPageDTO(Page<RenterDTO> page) {
        super(page);
    }
}
