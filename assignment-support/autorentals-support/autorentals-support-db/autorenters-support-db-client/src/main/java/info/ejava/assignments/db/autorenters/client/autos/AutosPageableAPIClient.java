package info.ejava.assignments.db.autorenters.client.autos;


import info.ejava.assignments.api.autorenters.client.autos.AutosAPIClient;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoSearchParams;
import info.ejava.examples.common.dto.paging.PageableDTO;
import info.ejava.examples.common.web.ServerConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class AutosPageableAPIClient extends AutosAPIClient implements AutosPageableAPI {

    public AutosPageableAPIClient(RestTemplate restTemplate, ServerConfig serverConfig, MediaType mediaType) {
        super(restTemplate, serverConfig, mediaType);
    }
    public AutosPageableAPIClient withRestTemplate(RestTemplate restTemplate) {
        ServerConfig serverConfig = new ServerConfig().withBaseUrl(getBaseUrl()).build();
        return new AutosPageableAPIClient(restTemplate, serverConfig, getMediaType());
    }

    /**
     * This query mechanism passes an example "probe" that is used to find matches
     * that exactly matches non-null fields
     * @param probe example for query
     * @param pageable sort, pageSize, and pageNumber info
     * @return page of matching autos
     */
    @Override
    public ResponseEntity<AutoPageDTO> queryAutosPaged(AutoDTO probe, Pageable pageable) {
        PageableDTO pageableDTO = PageableDTO.fromPageable(pageable);
        URI url = UriComponentsBuilder
                .fromUri(baseUrl)
                .queryParams(pageableDTO.getQueryParams())
                .path(AUTOS_PAGED_QUERY_PATH)
                .build().toUri();

        RequestEntity<AutoDTO> request = RequestEntity.post(url)
                .accept(getMediaType())
                .body(probe);
        return restTemplate.exchange(request, AutoPageDTO.class);
    }

    @Override
    public ResponseEntity<AutoPageDTO> searchAutosPaged(AutoSearchParams searchParams, Pageable pageable) {
        PageableDTO pageableDTO = PageableDTO.fromPageable(pageable);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUri(baseUrl)
                .path(AUTOS_PAGED_PATH)
                .queryParams(pageableDTO.getQueryParams());
        if (searchParams.getMinPassengersInclusive()!=null) {
            uriBuilder = uriBuilder.queryParam("minPassengers", searchParams.getMinPassengersInclusive());
        }
        if (searchParams.getMaxPassengersInclusive()!=null) {
            uriBuilder = uriBuilder.queryParam("maxPassengers", searchParams.getMaxPassengersInclusive());
        }
        if (searchParams.getMinDailyRateInclusive()!=null) {
            uriBuilder = uriBuilder.queryParam("minDailyRate", searchParams.getMinDailyRateInclusive());
        }
        if (searchParams.getMaxDailyRateExclusive()!=null) {
            uriBuilder = uriBuilder.queryParam("maxDailyRate", searchParams.getMaxDailyRateExclusive());
        }

        URI url = uriBuilder.build().toUri();

        RequestEntity<Void> request = RequestEntity.get(url)
                .accept(mediaType)
                .build();
        return restTemplate.exchange(request, AutoPageDTO.class);
    }

}