package info.ejava.assignments.db.autorenters.client.autos;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.examples.common.dto.paging.PageDTO;
import org.springframework.data.domain.Page;

/**
 * This DTO fully represents a page of results. It is included here
 * to be consistent with topics later in semester.
 */
public class AutoPageDTO extends PageDTO<AutoDTO> {
    protected AutoPageDTO() {}
    public AutoPageDTO(Page<AutoDTO> page) {
        super(page);
    }
}
