package info.ejava.assignments.db.autorenters.client.renters;

import info.ejava.assignments.api.autorenters.client.renters.RentersAPIClient;
import info.ejava.examples.common.dto.paging.PageableDTO;
import info.ejava.examples.common.web.ServerConfig;
import lombok.Getter;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * This class adds a pageable get method that is introduced later in the semester.
 */
public class RentersPageableAPIClient extends RentersAPIClient
        implements RentersPageableAPI {
    @Getter
    private final MediaType mediaType;

    public RentersPageableAPIClient(RestTemplate restTemplate, ServerConfig serverConfig, MediaType mediaType) {
        super(restTemplate, serverConfig, mediaType);
        this.mediaType = mediaType;
    }
    public RentersPageableAPIClient withRestTemplate(RestTemplate restTemplate) {
        ServerConfig serverConfig = new ServerConfig().withBaseUrl(getBaseUrl()).build();
        return new RentersPageableAPIClient(restTemplate, serverConfig, MediaType.APPLICATION_JSON);
    }

    @Override
    public ResponseEntity<RenterPageDTO> getRentersPaged(Pageable pageable) {
        URI rentersUrl = UriComponentsBuilder.fromUri(getBaseUrl()).path(BUYERS_PAGED_PATH).build().toUri();
        return getRenters(rentersUrl, RenterPageDTO.class, pageable);
    }

    private <T> ResponseEntity<T> getRenters(URI rentersUrl, Class<T> dtoType, Pageable pageable) {
        PageableDTO pageableDTO = PageableDTO.fromPageable(pageable);
        URI url = UriComponentsBuilder
                .fromUri(rentersUrl)
                .queryParams(pageableDTO.getQueryParams())
                .build().toUri();

        RestClient.RequestHeadersSpec<?> request = getRestClient()
                .get()
                .uri(url)
                .accept(getMediaType());
        return request.retrieve().toEntity(dtoType);
    }
}
