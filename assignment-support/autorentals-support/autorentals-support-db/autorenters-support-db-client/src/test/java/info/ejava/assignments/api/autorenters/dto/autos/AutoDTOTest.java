package info.ejava.assignments.api.autorenters.dto.autos;

import info.ejava.assignments.db.autorenters.client.autos.AutoPageDTO;
import info.ejava.examples.common.dto.DtoUtil;
import info.ejava.examples.common.dto.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.stream.Stream;

import static info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory.withId;
import static org.assertj.core.api.BDDAssertions.then;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
public class AutoDTOTest {
    private DtoUtil dtoUtil = new JsonUtil();
    private AutoDTOFactory autoFactory = new AutoDTOFactory();
    private Pageable pageable = PageRequest.of(0,10, Sort.by("renterAge"));

    private Stream<Arguments> renters() {
        return Stream.of(
                Arguments.of(new AutoPageDTO(new PageImpl<>(autoFactory.listBuilder().make(3, withId)))),
                Arguments.of(new AutoPageDTO(
                        new PageImpl<>(autoFactory.listBuilder().make(3, withId),pageable,3)
                ))
        );
    }

    @ParameterizedTest
    @MethodSource("renters")
    void can_marshal_unmarshal(/*given*/Object original) {
        //when
        String payload = dtoUtil.marshal(original);
        log.info("{}", payload);
        Object copy = dtoUtil.unmarshal(payload, original.getClass());
        //then
        then(copy).isEqualTo(original);
    }
}
