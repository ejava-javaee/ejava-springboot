package info.ejava.assignments.db.autorenters.svcjpa.autos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

public interface AutosJPARepository extends JpaRepository<AutoBO, String> {
    Page<AutoBO> findByPassengersBetween(int minPassengers, int maxPassengers, Pageable pageable);
    Page<AutoBO> findByDailyRateBetween(BigDecimal minDailyRate, BigDecimal maxDailyRate, Pageable pageable);
}
