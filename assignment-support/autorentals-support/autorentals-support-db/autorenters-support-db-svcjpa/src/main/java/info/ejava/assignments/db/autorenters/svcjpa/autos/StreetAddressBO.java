package info.ejava.assignments.db.autorenters.svcjpa.autos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StreetAddressBO {
    @Column(name="address_street")
    private String street;
    @Column(name="address_city")
    private String city;
    @Column(name="address_state")
    private String state;
    @Column(name="address_zip")
    private String zip;
}
