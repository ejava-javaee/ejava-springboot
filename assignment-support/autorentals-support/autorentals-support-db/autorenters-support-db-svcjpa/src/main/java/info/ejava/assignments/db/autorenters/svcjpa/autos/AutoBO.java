package info.ejava.assignments.db.autorenters.svcjpa.autos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import info.ejava.assignments.api.autorenters.dto.StreetAddressDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data

@Entity
@Table(name="AUTOS_HOME")
public class AutoBO {
    @PrePersist
    public void prePersist() {
        if (null==id) {
            id = UUID.randomUUID().toString();
        }
    }

    @Id //jakarta.persistence.Id
    private String id;
    private Integer passengers;
    private String fuelType;
    private BigDecimal dailyRate;
    private String make;
    private String model;
    @Embedded
    private StreetAddressBO location;
    private String username;
}
