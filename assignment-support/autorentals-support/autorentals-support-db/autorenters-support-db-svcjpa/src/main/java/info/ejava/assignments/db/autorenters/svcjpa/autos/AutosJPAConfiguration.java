package info.ejava.assignments.db.autorenters.svcjpa.autos;

import info.ejava.assignments.api.autorenters.svc.autos.AutosService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration(proxyBeanMethods = false)
@EnableJpaRepositories(basePackageClasses = {AutosJPARepository.class})
@EntityScan(basePackageClasses = {AutoBO.class})
@ConditionalOnClass({JpaRepository.class})
public class AutosJPAConfiguration {
    @Bean
    public AutosMapper autoMapper() {
        return new AutosMapper();
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public AutosService autosRepoService(AutosJPARepository jpaRepo, AutosMapper mapper) {
        return new AutosRepoServiceImpl(jpaRepo, mapper);
    }
}
