package info.ejava.assignments.db.autorenters.svcjpa.autos;

import info.ejava.assignments.api.autorenters.dto.StreetAddressDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;

public class AutosMapper {
    public StreetAddressBO map(StreetAddressDTO dto) {
        StreetAddressBO bo = null;
        if (null!=dto) {
            bo = StreetAddressBO.builder()
                    .street(dto.getStreet())
                    .city(dto.getCity())
                    .state(dto.getState())
                    .zip(dto.getZip())
                    .build();
        }
        return bo;
    }
    public StreetAddressDTO map(StreetAddressBO bo) {
        StreetAddressDTO dto = null;
        if (null!=bo) {
            dto = StreetAddressDTO.builder()
                    .street(bo.getStreet())
                    .city(bo.getCity())
                    .state(bo.getState())
                    .zip(bo.getZip())
                    .build();
        }
        return dto;
    }

    public AutoBO map(AutoDTO dto) {
        AutoBO bo = null;
        if (null!=dto) {
            bo = AutoBO.builder()
                    .id(dto.getId())
                    .make(dto.getMake())
                    .model(dto.getModel())
                    .fuelType(dto.getFuelType())
                    .passengers(dto.getPassengers())
                    .dailyRate(dto.getDailyRate())
                    .username(dto.getUsername())
                    .location(map(dto.getLocation()))
                    .build();
        }
        return bo;
    }

    public AutoDTO map(AutoBO bo) {
        AutoDTO dto = null;
        if (null!=bo) {
            dto = AutoDTO.builder()
                    .id(bo.getId())
                    .make(bo.getMake())
                    .model(bo.getModel())
                    .fuelType(bo.getFuelType())
                    .passengers(bo.getPassengers())
                    .dailyRate(bo.getDailyRate())
                    .username(bo.getUsername())
                    .location(map(bo.getLocation()))
                    .build();
        }
        return dto;
    }


}
