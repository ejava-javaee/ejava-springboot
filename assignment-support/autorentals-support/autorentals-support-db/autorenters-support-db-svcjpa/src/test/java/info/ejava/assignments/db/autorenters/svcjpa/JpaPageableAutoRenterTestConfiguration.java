package info.ejava.assignments.db.autorenters.svcjpa;

import info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPI;
import info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPIClient;
import info.ejava.examples.common.web.ServerConfig;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@TestConfiguration(proxyBeanMethods = true)
public class JpaPageableAutoRenterTestConfiguration {
    @Bean @Lazy
    public AutosPageableAPI authnAutosClient(RestTemplate authnUser, ServerConfig serverConfig) {
        return new AutosPageableAPIClient(authnUser, serverConfig, MediaType.APPLICATION_JSON);
    }

    @Bean @Lazy
    public AutosPageableAPI adminAutosClient(RestTemplate adminUser, ServerConfig serverConfig) {
        return new AutosPageableAPIClient(adminUser, serverConfig, MediaType.APPLICATION_JSON);
    }
}
