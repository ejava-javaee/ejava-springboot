package info.ejava.assignments.pageable.autorenters.svc.autos;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoSearchParams;
import info.ejava.assignments.api.autorenters.svc.autos.AutosController;
import info.ejava.assignments.api.autorenters.svc.autos.AutosService;
import info.ejava.assignments.db.autorenters.client.autos.AutoPageDTO;
import info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPI;
import info.ejava.examples.common.dto.paging.PageableDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This controller adds an additional endpoint not included in the
 * original API.
 */
public class AutosPageableController extends AutosController {
    private final AutosService service;
    public AutosPageableController(AutosService autosService) {
        super(autosService);
        service = autosService;
    }

    @RequestMapping(path= AutosPageableAPI.AUTOS_PAGED_QUERY_PATH,
            method= RequestMethod.POST,
            produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<AutoPageDTO> queryAutosPage(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestBody AutoDTO probe) {
        Pageable pageable = PageableDTO.of(pageNumber, pageSize, sort).toPageable();
        Page<AutoDTO> results = service.queryAutos(probe, pageable);

        AutoPageDTO autosPage = new AutoPageDTO(results);
        return ResponseEntity.ok(autosPage);
    }


    @GetMapping(path = AutosPageableAPI.AUTOS_PAGED_PATH,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<AutoPageDTO> searchAutosListPage(
            @RequestParam(value = "minDailyRate", required = false) Integer minDailyRate,
            @RequestParam(value = "maxDailyRate", required = false) Integer maxDailyRate,
            @RequestParam(value = "minPassengers", required = false) Integer minPassengers,
            @RequestParam(value = "maxPassengers", required = false) Integer maxPassengers,

            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "sort", required = false) String sort) {
        Pageable pageable = PageableDTO.of(pageNumber, pageSize, sort).toPageable();
        AutoSearchParams searchParams = AutoSearchParams.builder()
                .minDailyRateInclusive(minDailyRate)
                .maxDailyRateExclusive(maxDailyRate)
                .minPassengersInclusive(minPassengers)
                .maxPassengersInclusive(maxPassengers)
                .build();
        Page<AutoDTO> results = service.searchAutos(searchParams, pageable);

        AutoPageDTO resultDTO = new AutoPageDTO(results);
        return ResponseEntity.ok(resultDTO);
    }
}
