package info.ejava.assignments.db.autorenters.svc.rentals;


import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;

public interface RentalsMapper<D extends RentalDTO,B extends RentalBO> {
    public B map(D dto);
    public D map(B bo);
}
