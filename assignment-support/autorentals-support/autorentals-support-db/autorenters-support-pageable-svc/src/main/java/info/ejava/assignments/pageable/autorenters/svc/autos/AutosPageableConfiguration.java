package info.ejava.assignments.pageable.autorenters.svc.autos;

import info.ejava.assignments.api.autorenters.svc.autos.AutosAPIConfiguration;
import info.ejava.assignments.api.autorenters.svc.autos.AutosService;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(AutosAPIConfiguration.class)
public class AutosPageableConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public AutosPageableController autosPageableController(AutosService autosService) {
        return new AutosPageableController(autosService);
    }

    @Bean
    @ConditionalOnMissingBean
    public AutosPageableExceptionAdvice autosPageableExceptionAdvice() {
        return new AutosPageableExceptionAdvice();
    }
}
