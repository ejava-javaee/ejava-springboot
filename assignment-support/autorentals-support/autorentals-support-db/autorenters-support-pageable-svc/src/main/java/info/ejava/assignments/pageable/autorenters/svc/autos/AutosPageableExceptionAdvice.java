package info.ejava.assignments.pageable.autorenters.svc.autos;

import info.ejava.examples.common.web.BaseExceptionAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackageClasses = AutosPageableController.class)
public class AutosPageableExceptionAdvice extends BaseExceptionAdvice {
}
