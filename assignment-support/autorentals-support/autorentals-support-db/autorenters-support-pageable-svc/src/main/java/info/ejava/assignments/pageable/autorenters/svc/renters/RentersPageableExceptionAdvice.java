package info.ejava.assignments.pageable.autorenters.svc.renters;

import info.ejava.examples.common.web.BaseExceptionAdvice;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackageClasses = RentersPageableController.class)
public class RentersPageableExceptionAdvice extends BaseExceptionAdvice {
}
