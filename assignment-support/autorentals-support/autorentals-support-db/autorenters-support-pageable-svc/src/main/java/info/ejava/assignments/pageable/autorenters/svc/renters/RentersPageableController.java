package info.ejava.assignments.pageable.autorenters.svc.renters;

import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.renters.RentersController;
import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import info.ejava.assignments.db.autorenters.client.renters.RenterPageDTO;
import info.ejava.assignments.db.autorenters.client.renters.RentersPageableAPI;
import info.ejava.examples.common.dto.paging.PageableDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public class RentersPageableController extends RentersController {
    private final RentersService service;

    public RentersPageableController(RentersService rentersService) {
        super(rentersService);
        this.service = rentersService;
    }


    @GetMapping(path = RentersPageableAPI.BUYERS_PAGED_PATH,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RenterPageDTO> getRentersPage(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "sort", required = false) String sort) {
        Pageable pageable = PageableDTO.of(pageNumber, pageSize, sort).toPageable();
        Page<RenterDTO> result = service.getRenters(pageable);

        RenterPageDTO resultDTO = new RenterPageDTO(result);
        ResponseEntity<RenterPageDTO> response = ResponseEntity.ok(resultDTO);
        return response;
    }
}
