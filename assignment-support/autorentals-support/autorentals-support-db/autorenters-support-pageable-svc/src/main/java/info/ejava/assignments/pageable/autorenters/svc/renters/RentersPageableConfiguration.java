package info.ejava.assignments.pageable.autorenters.svc.renters;

import info.ejava.assignments.api.autorenters.svc.renters.RentersAPIConfiguration;
import info.ejava.assignments.api.autorenters.svc.renters.RentersService;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(RentersAPIConfiguration.class)
public class RentersPageableConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public RentersPageableController rentersPageableController(RentersService rentersService) {
        return new RentersPageableController(rentersService);
    }

    @Bean
    @ConditionalOnMissingBean
    public RentersPageableExceptionAdvice rentersPageableExceptionAdvice() {
        return new RentersPageableExceptionAdvice();
    }
}