package info.ejava.assignments.pageable.autorenters.svc.renters;

import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPIClient;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.api.autorenters.dto.renters.RenterListDTO;
import info.ejava.assignments.api.autorenters.svc.ProvidedApiAutoRenterTestConfiguration;
import info.ejava.assignments.db.autorenters.client.renters.RenterPageDTO;
import info.ejava.assignments.db.autorenters.client.renters.RentersPageableAPI;
import info.ejava.assignments.pageable.autorenters.svc.ProvidedPageableAutoRenterTestConfiguration;
import info.ejava.assignments.pageable.autorenters.svc.testapp.PageableSecurityTestConfiguration;
import info.ejava.assignments.security.autorenters.svc.ProvidedAuthorizationTestHelperConfiguration;
import info.ejava.examples.common.dto.JsonUtil;
import info.ejava.examples.common.dto.MessageDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes={
        ProvidedApiAutoRenterTestConfiguration.class,
        ProvidedAuthorizationTestHelperConfiguration.class,
        PageableSecurityTestConfiguration.class,
        ProvidedPageableAutoRenterTestConfiguration.class
        },
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootConfiguration
@EnableAutoConfiguration
@ActiveProfiles({"authorities","test"})
public class RentersPageableApiNTest {
    @Autowired
    private RenterDTOFactory renterFactory;
    @Autowired @Qualifier("authnRentersClient")
    private RentersPageableAPI authnClient;
    @Autowired @Qualifier("adminRentersClient")
    private RentersAPI adminClient;
    @Autowired @Qualifier("authn")
    private List<RestTemplate> authnClients;
    @Autowired
    private RestTemplate mgrUser;
    @Autowired @Qualifier("authnRentersClient")
    private RentersAPIClient renterClient;

    @BeforeEach
    void cleanup() {
        adminClient.removeAllRenters();
    }

    @Test
    void can_add_renter() {
        //given
        RenterDTO renter = renterFactory.make();
        //when
        ResponseEntity<RenterDTO> response = authnClient.createRenter(renter);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        RenterDTO addedRenter = response.getBody();
        then(addedRenter.getId()).isNotNull();
        then(addedRenter).isEqualTo(renter.withId(addedRenter.getId()));

        URI location = response.getHeaders().getLocation();
        URI expectedLocation = UriComponentsBuilder.fromUriString(RentersAPI.RENTER_PATH).build(addedRenter.getId());
        then(location.toString()).endsWith(expectedLocation.toString());
    }

    @Test
    void can_get_existing_renter() {
        //given
        RenterDTO existingRenter = populate(1).get(0);
        //when
        ResponseEntity<RenterDTO> response = authnClient.getRenter(existingRenter.getId());
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        RenterDTO responseRenter = response.getBody();
        then(responseRenter).isEqualTo(existingRenter);
    }

    @Test
    void get_non_existant_renter_returns_notfound() {
        //given
        String nonExistantId = "nonExistantId";
        RentersAPIClient mgr = renterClient.withRestTemplate(mgrUser);
        //when
        RestClientResponseException ex = catchThrowableOfType(()-> mgr.getRenter(nonExistantId),
                RestClientResponseException.class);
        //then
        then(ex.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        MessageDTO errorMsg = JsonUtil.instance().unmarshal(ex.getResponseBodyAsString(), MessageDTO.class);
        then(errorMsg.getTimestamp()).isNotNull();
        then(errorMsg.getDescription()).isEqualTo(String.format("Renter[%s] not found", nonExistantId));
    }

    @Test
    void existing_renter_returns_ok() {
        //given
        RenterDTO existingRenter = populate(1).get(0);
        //when
        ResponseEntity<Void> response = authnClient.hasRenter(existingRenter.getId());
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody()).isNull();
    }

    @Test
    void non_existing_renter_returns_notfound() {
        //given
        String nonExistingId = "nonExistingId";
        //when
        RestClientResponseException ex = catchThrowableOfType(
                () -> authnClient.hasRenter(nonExistingId),
                RestClientResponseException.class);
        //then
        then(ex.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void can_update_renter() {
        //given
        RenterDTO existingRenter = populate(1).get(0);
        LocalDate updatedDob = existingRenter.getDob().plusYears(1);
        RenterDTO updatedRenter = existingRenter.withDob(updatedDob);
        //when
        ResponseEntity<RenterDTO> response = authnClient.updateRenter(existingRenter.getId(), updatedRenter);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        RenterDTO responseRenter = response.getBody();
        then(responseRenter.getDob()).isEqualTo(updatedDob);
        then(responseRenter).isEqualTo(updatedRenter);
    }

    @Test
    void can_delete_a_renter() {
        //given
        List<RenterDTO> renters = new ArrayList<>(populate(3));
        RenterDTO existingBuyer = renters.remove(0);
        //when
        ResponseEntity<Void> response = authnClient.removeRenter(existingBuyer.getId());
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        renters.stream().forEach(h->{
            then(authnClient.hasRenter(h.getId()).getStatusCode()).isEqualTo(HttpStatus.OK);
        });
    }

    @Test
    void can_delete_all_renters() {
        //given
        List<RenterDTO> renters = new ArrayList<>(populate(3));
        //when
        ResponseEntity<Void> response = authnClient.removeAllRenters();
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        renters.stream().forEach(h->{
            then(renterExists(h.getId())).isFalse();
        });
    }

    private boolean renterExists(String id) {
        try {
            return authnClient.hasRenter(id).getStatusCode().equals(HttpStatus.OK);
        } catch (HttpClientErrorException.NotFound ex) {
            return false;
        }
    }

    /**
     * Buyers are unique -- so we can only create as many renters as we have acccounts
     */
    private List<RenterDTO> populate(int count) {
        List<RenterDTO> renters = new ArrayList<>();
        for (RestTemplate rt:authnClients) {
            RenterDTO renter = renterFactory.make();
            renter = renterClient.withRestTemplate(rt).createRenter(renter).getBody();
            renters.add(renter);
            if (renters.size() >= count) {
                break;
            }
        }
        return renters;
    }

    List<RenterDTO> queryBuyers = null;
    @Nested
    class query {
        @BeforeEach
        void cleanup() {
            if (null==queryBuyers) {
                authnClient.removeAllRenters();
                queryBuyers=populate(20);
            }
        }

        @Test
        void can_get_renters() {
            //given
            List<RenterDTO> expectedBuyers = queryBuyers;
            //when
            ResponseEntity<RenterListDTO> response = authnClient.getRenters(null, null);
            //then
            then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            List<RenterDTO> foundBuyers = response.getBody().getContents();
            then(foundBuyers).isNotNull();
            then(new HashSet<>(foundBuyers)).isEqualTo(new HashSet<>(expectedBuyers));
        }

        @Test
        void can_get_renters_paged_list() {
            //given
            List<RenterDTO> expectedBuyers = queryBuyers;
            int pageSize=3;
            int pageNum=0;
            List<RenterDTO> returnedBuyers = new ArrayList<>();
            do {
                //when
                ResponseEntity<RenterListDTO> response = authnClient.getRenters(pageNum, pageSize);
                RenterListDTO renters = response.getBody();
                //then
                then(response.getStatusCode().is2xxSuccessful()).isTrue();
                then(renters.getContents()).isNotNull();
                then(renters.getContents().size()).isLessThanOrEqualTo(pageSize);
                returnedBuyers.addAll(renters.getContents());
                if (renters.getContents().isEmpty()) {
                    break;
                }
                pageNum += 1;
            } while (true);

            then(new HashSet<>(returnedBuyers)).isEqualTo(new HashSet<>(expectedBuyers));
        }

        @Test
        void can_get_renters_paged_page() {
            //given
            List<RenterDTO> expectedBuyers = queryBuyers;
            int pageSize=3;
            int pageNum=0;
            List<RenterDTO> returnedBuyers = new ArrayList<>();
            do {
                //when
                ResponseEntity<RenterPageDTO> response = authnClient.getRentersPaged(PageRequest.of(pageNum, pageSize));
                RenterPageDTO renters = response.getBody();
                //then
                then(response.getStatusCode().is2xxSuccessful()).isTrue();
                then(renters.getContent()).isNotNull();
                then(renters.getPageNumber()).isEqualTo(pageNum);
                then(renters.getPageSize()).isEqualTo(pageSize);
                then(renters.getNumberOfElements()).isLessThanOrEqualTo(pageSize);
                then(renters.getTotalElements()).isEqualTo(expectedBuyers.size());
                then(renters.getSort()).isNull(); //no sort requested
                returnedBuyers.addAll(renters.getContent());
                if (!renters.hasNextPage()) {
                    break;
                }
                pageNum += 1;
            } while (true);

            then(new HashSet<>(returnedBuyers)).isEqualTo(new HashSet<>(expectedBuyers));
        }
    }
}
