package info.ejava.assignments.pageable.autorenters.svc.rentals;

import info.ejava.assignments.api.autorenters.client.autos.AutosAPI;
import info.ejava.assignments.api.autorenters.client.autos.AutosAPIClient;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPI;
import info.ejava.assignments.api.autorenters.client.renters.RentersAPIClient;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.autos.AutoSearchParams;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTOFactory;
import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenExceptionOfType;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.junit.jupiter.api.Assertions.assertThrows;

//@SpringBootTest(classes= { ...
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles({"test","authorities", "authorization"})
//@DisplayName("Part B2: Authorization")
public class RentalsSecurityNTest {
    @Autowired
    private AutoDTOFactory autoFactory;
    @Autowired
    private RenterDTOFactory renterFactory;
    @Autowired
    private AutosAPIClient autosClient;
    @Autowired
    private RentersAPIClient rentersClient;
    @Autowired
    private ApiTestHelper<RentalDTO> testHelper;
    @Autowired
    RestTemplate adminUser;
    @Autowired
    RestTemplate authnUser;
    @Autowired
    RestTemplate altUser;
    @Autowired
    RestTemplate proxyUser;
    @Autowired
    RestTemplate mgrUser;
    @Autowired Environment env;
    @Autowired ApplicationContext ctx;

    @BeforeEach
    void init() {
        given(env.getActiveProfiles()).as("missing required profile").contains("authorities", "authorization");
        autosClient.withRestTemplate(adminUser).removeAllAutos();
        rentersClient.withRestTemplate(adminUser).removeAllRenters();
        testHelper.withRestTemplate(adminUser).removeAllRentals();
    }

    @Test
    void method_security_enabled() {
        Map<String, Object> configs = ctx.getBeansWithAnnotation(EnableMethodSecurity.class);
        then(configs)
                .as(()->EnableMethodSecurity.class + "has not been enabled")
                .isNotEmpty();
        then(configs)
                .as("unexpected number of classes with annotation:" + EnableMethodSecurity.class)
                .hasSize(1);
        Object config = configs.values().iterator().next();
        EnableMethodSecurity annotation = config.getClass().getAnnotation(EnableMethodSecurity.class);
        then(annotation.prePostEnabled()).as("expression method security not enabled").isTrue();
    }

    AutoDTO given_a_auto() {
        return autosClient.withRestTemplate(adminUser).createAuto(autoFactory.make()).getBody();
    }
    RenterDTO given_a_renter(RestTemplate user) {
        return rentersClient.withRestTemplate(user).createRenter(renterFactory.make()).getBody();
    }

    @Nested
    class autos {
        private AutosAPIClient authnAutosClient;

        @Nested
        class authenticated_user {
            @BeforeEach
            void init() {
                authnAutosClient = autosClient.withRestTemplate(authnUser);
            }

            @Nested
            class may {
                @Test
                void create_auto() {
                    //given
                    AutoDTO validAuto = autoFactory.make();
                    //when
                    ResponseEntity<AutoDTO> response = authnAutosClient.createAuto(validAuto);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
                }

                @Test
                void modify_their_autos() {
                    //given
                    AutoDTO existingAuto = authnAutosClient.createAuto(autoFactory.make()).getBody();
                    AutoDTO modifiedAuto = existingAuto
                            .withPassengers(existingAuto.getPassengers()+1)
                            .withId(null);
                    //when
                    ResponseEntity<AutoDTO> response = authnAutosClient.updateAuto(existingAuto.getId(), modifiedAuto);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                    AutoDTO updatedAuto = response.getBody();
                    then(updatedAuto.getPassengers()).isEqualTo(modifiedAuto.getPassengers());
                }

                @Test
                void delete_their_autos() {
                    //given
                    AutoDTO existingAuto = authnAutosClient.createAuto(autoFactory.make()).getBody();
                    //when
                    ResponseEntity response = authnAutosClient.removeAuto(existingAuto.getId());
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    RestClientResponseException ex = assertThrows(RestClientResponseException.class,
                            () -> authnAutosClient.getAuto(existingAuto.getId()));
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
                }
            }

            @Nested
            class may_not {
                private AutosAPIClient altAutosClient;

                @BeforeEach
                void init() {
                    altAutosClient = autosClient.withRestTemplate(altUser);
                }

                @Test
                void modify_anothers_auto() {
                    //given
                    AutoDTO existingAuto = authnAutosClient.createAuto(autoFactory.make()).getBody();
                    AutoDTO modifiedAuto = existingAuto
                            .withPassengers(existingAuto.getPassengers()+1)
                            .withId(null);
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> altAutosClient.updateAuto(existingAuto.getId(), modifiedAuto));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }
                @Test
                void delete_anothers_auto() {
                    //given
                    AutoDTO existingAuto = authnAutosClient.createAuto(autoFactory.make()).getBody();
                    //when
                    HttpClientErrorException ex = assertThrows(HttpClientErrorException.class,
                            () -> altAutosClient.removeAuto(existingAuto.getId()));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }
                @Test
                void delete_all_autos() {
                    //given
                    AutoDTO existingAuto = authnAutosClient.createAuto(autoFactory.make()).getBody();
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnAutosClient.removeAllAutos());
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                    then(authnAutosClient.getAuto(existingAuto.getId()).getStatusCode()).isEqualTo(HttpStatus.OK);
                    then(authnAutosClient.searchAutos(AutoSearchParams.builder()
                                    .pageNumber(0)
                                    .pageSize(1)
                                    .build()).getBody().getContents()).isNotEmpty();
                }
            }
        }

        @Nested
        class unauthenticated_user {
            @Test
            void may_not_create_auto() {
                //given
                AutoDTO validAuto = autoFactory.make();
                //when
                RestClientResponseException ex = assertThrows(RestClientResponseException.class,
                        () -> autosClient.createAuto(validAuto));
                //then
                then(ex.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
            }
        }

        @Nested
        class admin_user {
            private AutosAPI adminAutosClient;

            @BeforeEach
            void init() {
                adminAutosClient = autosClient.withRestTemplate(adminUser);
            }
            @Test
            void can_delete_all_autos() {
                //given
                AutoDTO existingAuto = adminAutosClient.createAuto(autoFactory.make()).getBody();
                //when
                ResponseEntity response = adminAutosClient.removeAllAutos();
                //then
                then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
            }
        }
    }


    @Nested
    class renters {
        @Nested
        class authenticated_user {
            private RentersAPI authnRentersClient;

            @BeforeEach
            void init() {
                authnRentersClient = rentersClient.withRestTemplate(authnUser);
            }

            @Nested
            class may {
                @Test
                void create_their_renter() {
                    //given
                    RenterDTO validRenter = renterFactory.make();
                    //when
                    ResponseEntity response = authnRentersClient.createRenter(validRenter);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
                }

                @Test
                void modify_their_renter() {
                    //given
                    RenterDTO existingRenter = authnRentersClient.createRenter(renterFactory.make()).getBody();
                    RenterDTO modifiedRenter = existingRenter.withFirstName(existingRenter.getFirstName() + " modified");
                    //when
                    ResponseEntity<RenterDTO> response = authnRentersClient.updateRenter(existingRenter.getId(), modifiedRenter);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                    RenterDTO updatedRenter = response.getBody();
                    then(updatedRenter.getFirstName()).isEqualTo(modifiedRenter.getFirstName()).contains("modified");
                }

                @Test
                void delete_their_renter() {
                    //given
                    RenterDTO existingRenter = authnRentersClient.createRenter(renterFactory.make()).getBody();
                    //when
                    ResponseEntity response = authnRentersClient.removeRenter(existingRenter.getId());
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    thenExceptionOfType(HttpClientErrorException.NotFound.class)
                            .isThrownBy(() -> authnRentersClient.getRenter(existingRenter.getId()))
                            .withMessageContaining("not found");
                }
            }

            @Nested
            @DirtiesContext
            class may_not {
                RenterDTO anotherRenter;
                RentersAPI adminRentersClient;

                @BeforeEach
                void init() {
                    adminRentersClient = rentersClient.withRestTemplate(adminUser);
//                    adminRentersClient.removeAllRenters();
                    anotherRenter = rentersClient.withRestTemplate(altUser).createRenter(renterFactory.make()).getBody();
                }

                @Test
                void modify_anothers_renter() {
                    //given
                    RenterDTO modifiedRenter = anotherRenter.withFirstName(anotherRenter.getFirstName() + " modified");
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnRentersClient.updateRenter(anotherRenter.getId(), modifiedRenter));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }

                @Test
                void delete_anothers_renter() {
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnRentersClient.removeRenter(anotherRenter.getId()));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }

                @Test
                void delete_all_renters() {
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnRentersClient.removeAllRenters());
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                    then(adminRentersClient.getRenters(0, 1).getBody().getContents()).isNotEmpty();
                }
            }
        }

        @Nested
        class unauthenticated_user {
            @Nested
            class may_not {
                @Test
                void create_renter() {
                    //given
                    RenterDTO validRenter = renterFactory.make();
                    //when
                    RestClientResponseException ex = Assertions.assertThrows(RestClientResponseException.class,
                            () -> rentersClient.createRenter(validRenter));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
                }
            }
        }

        @Nested
        class admin_user {
            private RentersAPI adminRenterClient;

            @BeforeEach
            void init() {
                adminRenterClient = rentersClient.withRestTemplate(adminUser);
            }

            @Nested
            class can {
                @Test
                void delete_all_renters() {
                    //given
                    given_a_renter(authnUser);
                    then(adminRenterClient.getRenters(0, 1).getBody().getContents()).isNotEmpty();
                    //when
                    ResponseEntity response = adminRenterClient.removeAllRenters();
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    then(adminRenterClient.getRenters(0, 1).getBody().getContents()).isEmpty();
                }
            }
        }
    }

    @Nested
    class rentals {
        private TimePeriod nextTimePeriod = new TimePeriod(LocalDate.now(), 7);

        protected TimePeriod given_a_time_period() {
            TimePeriod current = nextTimePeriod;
            nextTimePeriod = nextTimePeriod.next();
            return current;
        }

        AutoDTO given_an_auto() {
            return autosClient.withRestTemplate(adminUser).createAuto(autoFactory.make()).getBody();
        }
        RenterDTO given_a_renter(RestTemplate user) {
            return rentersClient.withRestTemplate(user).createRenter(renterFactory.make()).getBody();
        }
        RentalDTO given_a_proposal(RenterDTO renter) {
            renter = Objects.requireNonNull(renter);
            TimePeriod timePeriod = given_a_time_period();
            AutoDTO auto = given_an_auto();
            return testHelper.makeProposal(auto, renter, timePeriod);
        }
        RentalDTO given_a_proposal(RestTemplate user) {
            AutoDTO auto = given_an_auto();
            RenterDTO renter = given_a_renter(user);
            TimePeriod timePeriod = given_a_time_period();
            return testHelper.makeProposal(auto, renter, timePeriod);
        }
        RentalDTO given_a_contract(RestTemplate user) {
            RentalDTO proposal = given_a_proposal(user);
            return testHelper.withRestTemplate(user).createContract(proposal).getBody();
        }

        @Nested
        class authenticated_users {
            AutosAPI authnAutosClient;
            RentersAPI authnRentersClient;
            ApiTestHelper<RentalDTO> authnHelper;

            @BeforeEach
            void init() {
                authnAutosClient = autosClient.withRestTemplate(authnUser);
                authnRentersClient = rentersClient.withRestTemplate(authnUser);
                authnHelper = testHelper.withRestTemplate(authnUser);
            }

            @Nested
            class may {
                @Test
                void create_contract_for_existing_auto() {
                    //given
                    RentalDTO proposal = given_a_proposal(authnUser);
                    //when
                    ResponseEntity<RentalDTO> response = authnHelper.createContract(proposal);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
                }

                @Test
                void delete_their_own_contract() {
                    //given
                    RentalDTO contract = given_a_contract(authnUser);
                    String rentalId = testHelper.getRentalId(contract);
                    //when
                    ResponseEntity<Void> response = authnHelper.removeRental(rentalId);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                }
            }

            @Nested
            class may_not {
                AutosAPI altAutoClient;
                ApiTestHelper<RentalDTO> altHelper;

                @BeforeEach
                void init() {
                    altAutoClient = autosClient.withRestTemplate(altUser);
                    altHelper = testHelper.withRestTemplate(altUser);
                }

                @Test
                void create_contract_for_other_user() {
                    //given
                    RentalDTO proposal = given_a_proposal(altUser);
                    //when
                    HttpClientErrorException ex = assertThrows(HttpClientErrorException.class,
                            () -> authnHelper.createContract(proposal));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }

                @Test
                void delete_anothers_contract() {
                    //given
                    RentalDTO contract = given_a_contract(altUser);
                    String rentalId = testHelper.getRentalId(contract);
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnHelper.removeRental(rentalId));
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                }

                @Test
                void cannot_delete_all_registrations() {
                    //given
                    RentalDTO rental = given_a_contract(authnUser);
                    String rentalId = testHelper.getRentalId(rental);
                    //when
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> authnHelper.removeAllRentals());
                    //then
                    then(ex.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
                    then(authnHelper.getRentalById(rentalId).getStatusCode()).isEqualTo(HttpStatus.OK);
                }
            }
        }

        @Nested
        class proxy_user {
            ApiTestHelper<RentalDTO> proxyHelper;

            @BeforeEach
            void init() {
                proxyHelper = testHelper.withRestTemplate(proxyUser);
            }

            @Nested
            class may {
                @Test
                void create_contract_for_other_user() {
                    //given
                    RenterDTO renter = given_a_renter(authnUser);
                    RentalDTO proposal = given_a_proposal(renter);
                    String rentalId = testHelper.getRentalId(proposal);
                    //when
                    ResponseEntity<RentalDTO> response = proxyHelper.createContract(proposal);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                    String renterId = testHelper.getRenterId(response.getBody());
                    then(renterId).isEqualTo(renter.getId());
                }
            }

            @Nested
            class may_not {

            }
        }

        @Nested
        class mgr {
            ApiTestHelper<RentalDTO> mgrHelper;

            @BeforeEach
            void init() {
                mgrHelper = testHelper.withRestTemplate(mgrUser);
            }

            @Nested
            class may {
                @Test
                void create_contract_for_other_user() {
                    //given
                    RenterDTO renter = given_a_renter(authnUser);
                    RentalDTO proposal = given_a_proposal(renter);
                    //when
                    ResponseEntity<RentalDTO> response = mgrHelper.createContract(proposal);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                    String renterId = testHelper.getRenterId(response.getBody());
                    then(renterId).isEqualTo(renter.getId());
                }

                @Test
                void delete_contract() {
                    //given
                    RentalDTO proposal = given_a_proposal(authnUser);
                    String rentalId = testHelper.getRentalId(proposal);
                    //when
                    ResponseEntity response = mgrHelper.removeRental(rentalId);
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> testHelper.withRestTemplate(authnUser).getRentalById(rentalId));
                    then(ex.getStatusCode()).isEqualTo((HttpStatus.NOT_FOUND));
                }
            }

            @Nested
            class may_not {
            }
        }
        @Nested
        class admin {
            ApiTestHelper<RentalDTO> adminHelper;

            @BeforeEach
            void init() {
                adminHelper = testHelper.withRestTemplate(adminUser);
            }

            @Nested
            class may {
                @Test
                void delete_all_rentals() {
                    //given
                    RentalDTO contract = given_a_contract(authnUser);
                    String rentalId = testHelper.getRentalId(contract);
                    //when
                    ResponseEntity response = adminHelper.removeAllRentals();
                    //then
                    then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> adminHelper.getRentalById(rentalId));
                    then(ex.getStatusCode()).isEqualTo((HttpStatus.NOT_FOUND));
                }
                @Test
                void inherit_delete_contract_authority_from_mgr() {
                    //given
                    RentalDTO contract = given_a_contract(authnUser);
                    String rentalId = testHelper.getRentalId(contract);
                    //when
                    Assertions.assertDoesNotThrow(()->{
                        ResponseEntity response = adminHelper.removeRental(rentalId);
                        //then
                        then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
                    },"removeRental as ADMIN failed, check role inheritance");

                    //then
                    HttpStatusCodeException ex = assertThrows(HttpStatusCodeException.class,
                            () -> testHelper.withRestTemplate(authnUser).getRentalById(rentalId));
                    then(ex.getStatusCode()).isEqualTo((HttpStatus.NOT_FOUND));
                }
                @Test
                void inherit_create_contract_for_another_user() {
                    //given
                    RenterDTO renter = given_a_renter(authnUser);
                    RentalDTO proposal = given_a_proposal(altUser);
                    //when
                    Assertions.assertDoesNotThrow(()-> {
                        ResponseEntity<RentalDTO> response = adminHelper.createContract(proposal);
                        //then
                        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
                        String renterId = testHelper.getRenterId(response.getBody());
                        then(renterId).isEqualTo(renter.getId());
                    },"purchase as ADMIN failed, check role inheritance");
                }
            }

            @Nested
            class may_not {}
        }
    }
}
