package info.ejava.assignments.pageable.autorenters.svc.autos;

import info.ejava.assignments.api.autorenters.client.autos.AutosAPI;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTOFactory;
import info.ejava.assignments.api.autorenters.dto.autos.AutoListDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoSearchParams;
import info.ejava.assignments.pageable.autorenters.svc.ProvidedPageableAutoRenterTestConfiguration;
import info.ejava.assignments.pageable.autorenters.svc.testapp.PageableSecurityTestConfiguration;
import info.ejava.assignments.security.autorenters.svc.ProvidedAuthorizationTestHelperConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;

import static org.assertj.core.api.BDDAssertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes={
        ProvidedAuthorizationTestHelperConfiguration.class,
        PageableSecurityTestConfiguration.class,
        ProvidedPageableAutoRenterTestConfiguration.class
    },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootConfiguration
@EnableAutoConfiguration
@ActiveProfiles({"authorities","test"})
public class AutosRepoNTest {
    @Autowired
    private AutoDTOFactory autoFactory;
    @Autowired @Qualifier("authnAutosClient")
    private AutosAPI authnClient;

    @BeforeEach
    @AfterEach
    void cleanup(@Autowired AutosAPI adminAutosClient) {
        adminAutosClient.removeAllAutos();
    }

    @Test
    void get_auto() {
        //verify
        assertThrows(HttpClientErrorException.NotFound.class,
                () -> authnClient.hasAuto("aAutoId"));
    }

    @Test
    void check_auto_exists() {
        //when
        HttpClientErrorException ex = catchThrowableOfType(
                () -> authnClient.hasAuto("aAutoId"),
                HttpClientErrorException.class);
        //then
        then(ex).as("no exception thrown").isNotNull();
        then(ex.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void get_autos() {
        //when
        ResponseEntity<AutoListDTO> response = authnClient
                .searchAutos(AutoSearchParams.builder().build().page(0, 1));
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void add_auto() {
        //given
        AutoDTO auto = autoFactory.make();
        //when
        ResponseEntity<AutoDTO> response = authnClient.createAuto(auto);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        then(response.getBody().getId()).isNotNull();
    }

    @Test
    void update_auto() {
        //given
        AutoDTO auto = authnClient.createAuto(autoFactory.make()).getBody();
        int modifiedPassengers = auto.getPassengers()+1;
        AutoDTO modifiedAuto = auto.withPassengers(modifiedPassengers);
        //when
        ResponseEntity<AutoDTO> response = authnClient.updateAuto(auto.getId(), modifiedAuto);
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        then(response.getBody().getPassengers()).isEqualTo(modifiedPassengers);
    }
    @Test
    void remove_their_auto() {
        //given
        AutoDTO auto = authnClient.createAuto(autoFactory.make()).getBody();
        //when
        ResponseEntity<Void> response = authnClient.removeAuto(auto.getId());
        //then
        then(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}
