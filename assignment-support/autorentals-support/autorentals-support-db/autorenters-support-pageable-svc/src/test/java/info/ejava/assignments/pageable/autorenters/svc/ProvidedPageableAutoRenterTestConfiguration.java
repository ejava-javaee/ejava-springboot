package info.ejava.assignments.pageable.autorenters.svc;

import info.ejava.assignments.db.autorenters.client.renters.RentersPageableAPI;
import info.ejava.assignments.db.autorenters.client.renters.RentersPageableAPIClient;
import info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPI;
import info.ejava.examples.common.web.ServerConfig;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@TestConfiguration(proxyBeanMethods = true)
public class ProvidedPageableAutoRenterTestConfiguration {
    @Bean @Lazy
    public AutosPageableAPI authnAutosClient(RestTemplate authnUser, ServerConfig serverConfig) {
        return new info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPIClient(authnUser, serverConfig, MediaType.APPLICATION_JSON);
    }

    @Bean @Lazy
    public AutosPageableAPI adminAutosClient(RestTemplate adminUser, ServerConfig serverConfig) {
        return new info.ejava.assignments.db.autorenters.client.autos.AutosPageableAPIClient(adminUser, serverConfig, MediaType.APPLICATION_JSON);
    }

    @Bean @Lazy
    public RentersPageableAPI authnRentersClient(RestTemplate authnUser, ServerConfig serverConfig) {
        return new RentersPageableAPIClient(authnUser, serverConfig, MediaType.APPLICATION_JSON);
    }

    @Bean @Lazy
    public RentersPageableAPI adminRentersClient(RestTemplate adminUser, ServerConfig serverConfig) {
        return new RentersPageableAPIClient(adminUser, serverConfig, MediaType.APPLICATION_JSON);
    }
}
