package info.ejava.assignments.autoconfig.rentals;

import org.assertj.core.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * This test verifies which component was selected to be injected.
 * It uses relection to get around the fact that it does not know the
 * solution's package and classnames -- just obvious patterns.
 *
 * We will cover testing soon and reflection several weeks after that
 */
@SpringBootConfiguration
//prevent the AutoConfiguration classes from being in the component scanpath
//make sure they are picked up during the autoconfiguration phase
@ComponentScan(basePackages = "info.ejava_student",
        excludeFilters = {
                @ComponentScan.Filter(type=FilterType.ASPECTJ, pattern = "*..*Auto*()"),
                @ComponentScan.Filter(type=FilterType.ASPECTJ, pattern = "*..*Auto*()"),
                @ComponentScan.Filter(type= FilterType.ANNOTATION, classes= ConditionalOnClass.class)
        })
@EnableAutoConfiguration
public class RentalsConfigTest {
    Object component;
    Object rentalsService;

    class TestBase {
        String implementation;
        Set<String> implementations=new HashSet<>();

        @BeforeEach
        void init(@Autowired ApplicationContext ctx) throws Exception {
            component = ctx.getBean("appCommand");

            rentalsService = get(component, "rentalsService", Object.class);
            Class<?> serviceInterface = getReturnType(component, "rentalsService");
            System.out.println("impl interface: " + serviceInterface);
            implementations = getImplementations(serviceInterface);
            implementation = getCurrentImplementation(rentalsService);
            System.out.println("impls classes available: " + implementations);
            System.out.println("impl=" + implementation);
        }
        String getCurrentImplementation(Object rentalsService) {
            Object rentalDTO = get(rentalsService,"randomRental", Object.class);
            String name = get(rentalDTO, "name", String.class);
            if (null!=name) {
                if (name.toLowerCase().contains("auto")) return "autos";
                if (name.toLowerCase().contains("tool")) return "tools";
            }
            return null;
        }

        Set<String> getImplementations(Class<?> serviceInterface) {
            if (null==serviceInterface) {
                return Set.of();
            }
            ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(serviceInterface));
            Set<BeanDefinition> candidates = scanner.findCandidateComponents("info.ejava_student");
            return candidates.stream()
                    .map(bean->bean.getBeanClassName())//returns full package name
                    .map(fullName->{ //get classname
                        System.out.println("impl class found: " + fullName); //
                        String[] elements=fullName.split("\\.");
                        return elements[elements.length-1];
                    })
                    .map(name->{
                        if (name.toLowerCase().contains("auto")) return "autos";
                        if (name.toLowerCase().contains("tool")) return "tools";
                        return null;})
                    .filter(type->null!=type)
                    .collect(Collectors.toSet());
        }
    }

    @Nested
    @SpringBootTest
    class no_properties extends TestBase {
        @Test
        void injects_tools_when_only_tools_in_path() {
            Assumptions.assumeThat(implementations).isEqualTo(Set.of("tools"));
            then(implementation).as("impl class found in classpath, but not injected").isEqualTo("tools");
        }
        @Test
        void injects_autos_when_autos_in_path() {
            Assumptions.assumeThat(implementations).contains("autos");
            then(implementation).as("impl class found in classpath, but not injected").isEqualTo("autos");
        }
        @Test
        void no_injection_if_neither_in_path() {
            Assumptions.assumeThat(implementations).isEmpty();
            then(implementation).as("unexpected impl").isNull();
        }
    }

    @Nested
    @SpringBootTest(properties = {"rentals.preference=tools"})
    class preference_is_tools extends TestBase {
        @Test
        void injects_tools_when_tools_in_path() {
            Assumptions.assumeThat(implementations).contains("tools");
            then(implementation).as("impl class found in classpath, but not injected").isEqualTo("tools");
        }
        @Test
        void injects_tools_unless_only_autos_in_path() {
            Assumptions.assumeThat(implementations).isEqualTo(Set.of("autos"));
            then(implementation).as("impl class found in classpath, but not injected").isEqualTo("autos");
        }
        @Test
        void injects_tools_unless_neither_in_path() {
            Assumptions.assumeThat(implementations).isEmpty();
            then(implementation).as("unexpected impl").isNull();
        }
    }


    @Nested
    @SpringBootTest(properties = {"rentals.active=false"})
    class rentals_not_active extends TestBase {
        @Test
        void no_bean_injected() {
            then(rentalsService).as("unexpected injection").isNull();
        }
    }

    private int getId(Object obj) {
        return (int) ReflectionTestUtils.invokeGetterMethod(obj, "id");
        //return get(obj, "id", Integer.class);
    }
    private <T> T get(Object obj, String property, Class<T> type) {
        if (null==obj) {
            return null;
        }
        String getter = "get" + StringUtils.capitalize(property);
        try {
            Method getId = obj.getClass().getDeclaredMethod(getter);
            return (T) getId.invoke(obj);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException ex) {
            throw new IllegalStateException(String.format("unable to locate %s method in %s", getter, obj), ex);
        }
    }

    private Class<?> getReturnType(Object obj, String property) {
        if (null==obj) {
            return null;
        }
        String getter = "get" + StringUtils.capitalize(property);
        try {
            Method getId = obj.getClass().getDeclaredMethod(getter);
            return getId.getReturnType();
        } catch (NoSuchMethodException ex) {
            throw new IllegalStateException(String.format("unable to locate property %s in obj %s", property, obj),ex);
        }
    }
}
