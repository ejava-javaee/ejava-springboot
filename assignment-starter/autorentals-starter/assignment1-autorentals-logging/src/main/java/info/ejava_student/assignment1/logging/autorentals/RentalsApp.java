package info.ejava_student.assignment1.logging.autorentals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalsApp {
    public static void main(String[] args) {
        SpringApplication.run(RentalsApp.class, args);
    }
}
