package info.ejava_student.starter.assignment1.beanfactory.rentals;

public interface RentalsService {
    RentalDTO getRandomRental();
}
