package info.ejava_student.starter.assignment1.propertysource.rentals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertySourceApp {
    public static void main(String[] args) {
        SpringApplication.run(PropertySourceApp.class, args);
    }
}
