package info.ejava_student.starter.assignment3.security.config;


import info.ejava.assignments.security.autorenters.svc.Accounts;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration(proxyBeanMethods = false)
public class SecurityConfiguration {

    @Configuration(proxyBeanMethods = false)
    @Profile("anonymous-access")
    public static class PartA1_AnonymousAccess {
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
            //TODO
            return http.build();
        }
    }


    @Configuration(proxyBeanMethods = false)
    @Profile({"authenticated-access", "userdetails"})
    public static class PartA2_AuthenticatedAccess {
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
            //TODO
            return http.build();
        }
    }


    @Configuration(proxyBeanMethods = false)
    @Profile("nosecurity")
    public static class PartA2b_NoSecurity {
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
            //TODO
            return http.build();
        }
    }


    @Configuration(proxyBeanMethods = false)
    @Profile({"nosecurity","userdetails", "authorities", "authorization"})
    public static class PartA3_UserDetailsPart {
        //@Bean
        public PasswordEncoder passwordEncoder() {
            //TODO
            return null;
        }

        //@Bean
        public UserDetailsService userDetailsService(PasswordEncoder encoder, Accounts accounts) {
            //TODO
            return null;
        }
    }


    @Configuration(proxyBeanMethods = false)
    @Profile("authorization")
    //enable global method security for prePostEnabled
    public static class PartA_Authorization {
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
            //TODO
            return http.build();
        }

        @Bean
        public RoleHierarchy roleHierarchy() {
            return RoleHierarchyImpl.withDefaultRolePrefix()
                    //TODO
                    .build();
        }
    }
}
