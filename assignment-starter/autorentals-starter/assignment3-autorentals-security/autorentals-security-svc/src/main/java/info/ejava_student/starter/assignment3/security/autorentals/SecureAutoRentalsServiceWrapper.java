package info.ejava_student.starter.assignment3.security.autorentals;

import info.ejava.assignments.api.autorenters.dto.rentals.SearchParams;
import info.ejava_student.starter.assignment2.api.autorentals.AutoRentalsService;
import info.ejava_student.starter.assignment2.api.autorentals.client.AutoRentalDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@RequiredArgsConstructor
@Slf4j
public class SecureAutoRentalsServiceWrapper implements AutoRentalsService {
    private final AutoRentalsService serviceImpl;
    //...
    //anything unique to this solution can be @Autowired

    public AutoRentalDTO createAutoRental(AutoRentalDTO proposedRental) {
        //AutoRentalDTO rental = serviceImpl.createAutoRental(proposedRental);
        return null;
    }

    public AutoRentalDTO updateAutoRental(String autoRentalId, AutoRentalDTO rental) {
        //return serviceImpl.updateAutoRental(autoRentalId, purchaseInfo);
        return null;
    }

    public AutoRentalDTO getAutoRental(String id) {
        //return serviceImpl.getAutoRental(id);
        return null;
    }

    public Page<AutoRentalDTO> findAutoRentals(SearchParams searchParams, Pageable pageable) {
        //return serviceImpl.findAutoRentals(searchParams, pageable);
        return null;
    }

    public void deleteAutoRentalDTO(String id) {
        //serviceImpl.deleteAutoRentalDTO(id);
    }

    public void deleteAllAutoRentals() {
        //serviceImpl.deleteAllAutoRentals();
    }
}
