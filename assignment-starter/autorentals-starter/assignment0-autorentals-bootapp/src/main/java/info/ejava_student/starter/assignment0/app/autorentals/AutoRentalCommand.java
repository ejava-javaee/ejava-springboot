//package info.ejava_student.starter.assignment0.app.autorentals;

import org.springframework.boot.CommandLineRunner;

//@Component
public class AutoRentalCommand implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("AutoRental has started");
    }
}
