package info.ejava_student.starter.assignment2.api.autorentals.impl;

import info.ejava.assignments.api.autorenters.dto.StreetAddressDTO;
import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.SearchParams;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import info.ejava_student.starter.assignment2.api.autorentals.client.AutoRentalDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

//TODO: implement this component for MyAutoRentalsAPINTest
/**
 * This class maps the RentalDTO marker factory and interface calls as well
 * as server API calls to solution-specific methods.
 *
 * "make" methods are client-side POJO factories.
 * "getter/setter" methods translate between requests and your concrete rental DTO class.
 * server-side calls return ResponseEntity except the finder(s)
 * "finder(s)" simply return the List of DTOs
 */
public class ApiTestHelperImpl implements ApiTestHelper<AutoRentalDTO> {
    //you will need a client instance to call your server-side implementation

    //you may need a reusable mechanism to construct DTO instances

    @Override
    public ApiTestHelper<AutoRentalDTO> withRestTemplate(RestTemplate restTemplate) {
        return null; //new instance of this helper, with clients using provided restTemplate
    }

    @Override
    public AutoRentalDTO makeProposal(AutoDTO auto, RenterDTO renter, TimePeriod timePeriod) {
        return null;
    }
    public AutoRentalDTO makePopulatedFake() { //create instance with non-null, fake values to test getters
        return null;
    }

    @Override
    public String getRentalId(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public void setRentalId(AutoRentalDTO autoRental, String rentalId) {

    }

    @Override
    public String getAutoId(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public void setAutoId(AutoRentalDTO autoRental, String autoId) {

    }

    @Override
    public String getRenterId(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public void setRenterId(AutoRentalDTO autoRental, String renterId) {

    }

    @Override
    public LocalDate getStartDate(AutoRentalDTO autoRental) {
        return null;
    }
    @Override
    public void setStartDate(AutoRentalDTO autoRental, LocalDate startDate) {
    }

    @Override
    public LocalDate getEndDate(AutoRentalDTO autoRental) {
        return null;
    }
    @Override
    public void setEndDateDate(AutoRentalDTO autoRental, LocalDate endDate) {
    }

    @Override
    public String getAutoMakeModel(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public String getRenterName(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public BigDecimal getAmount(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public int getRenterAge(AutoRentalDTO autoRental) {
        return 0;
    }

    @Override
    public StreetAddressDTO getStreetAddress(AutoRentalDTO autoRental) {
        return null;
    }

    @Override
    public TimePeriod getTimePeriod(AutoRentalDTO autoRental) {
        return ApiTestHelper.super.getTimePeriod(autoRental);
    }

    ////////// calls to server-side API


    @Override
    public ResponseEntity<AutoRentalDTO> createContract(AutoRentalDTO proposedRental) {
        return null;
    }

    @Override
    public ResponseEntity<AutoRentalDTO> modifyContract(AutoRentalDTO proposedRental) {
        return null;
    }

    @Override
    public ResponseEntity<AutoRentalDTO> getRental(AutoRentalDTO rentalContract) {
        return null;
    }

    @Override
    public ResponseEntity<AutoRentalDTO> getRentalById(String rentalId) {
        return null;
    }

    @Override
    public List<AutoRentalDTO> findRentalsBy(SearchParams searchParams) {
        return null;
    }

    @Override
    public ResponseEntity<Void> removeRental(String rentalId) {
        return null;
    }

    @Override
    public ResponseEntity<Void> removeAllRentals() {
        return null;
    }
}
