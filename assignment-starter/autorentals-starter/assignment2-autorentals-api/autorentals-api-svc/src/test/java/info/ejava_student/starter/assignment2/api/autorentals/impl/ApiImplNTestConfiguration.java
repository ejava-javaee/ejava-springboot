package info.ejava_student.starter.assignment2.api.autorentals.impl;

import info.ejava.assignments.api.autorenters.svc.rentals.ApiTestHelper;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

//TODO: implement this @TestConfiguration for MyAutoRentalsAPINTest
/**
 * This test configuration will provide a factory bean for
 * the test-helper and any additional injections the test-helper
 * requires.
 */
@TestConfiguration(proxyBeanMethods = false)
public class ApiImplNTestConfiguration {
    @Bean
    public ApiTestHelper testHelper(/*dependencies of your helper*/) {
        return new ApiTestHelperImpl(/*required args*/);
    }

    //you will need an initial client component @Bean

    //you may want a DTO factory @Bean
}
