package info.ejava_student.starter.assignment2.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoRentalsApiApp {
    public static void main(String...args) {
        SpringApplication.run(AutoRentalsApiApp.class, args);
    }
}
