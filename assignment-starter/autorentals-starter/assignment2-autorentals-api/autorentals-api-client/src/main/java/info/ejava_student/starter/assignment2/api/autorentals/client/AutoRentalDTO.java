package info.ejava_student.starter.assignment2.api.autorentals.client;

import info.ejava.assignments.api.autorenters.dto.autos.AutoDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.RentalDTO;
import info.ejava.assignments.api.autorenters.dto.rentals.TimePeriod;
import info.ejava.assignments.api.autorenters.dto.renters.RenterDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AutoRentalDTO implements RentalDTO {
    private String remove_me = "don't be empty";

    public AutoRentalDTO(AutoDTO auto, RenterDTO renter, TimePeriod timePeriod) {
        setAuto(auto);
        setRenter(renter);
        setTimePeriod(timePeriod);
    }

    public void setAuto(AutoDTO auto) {
    }

    public void setRenter(RenterDTO renter) {
    }

    public void setTimePeriod(TimePeriod timePeriod) {
    }
}
